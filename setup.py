#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

import bm16


def get_readme():
    _dir = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.join(_dir, "README.md"), "r") as f:
        long_description = f.read()
    return long_description


def main():
    """The main entry point."""
    kwargs = dict(
        name="bm16",
        use_scm_version=True,
        setup_requires=['setuptools_scm'],
        #version=bm16.__version__,
        packages=bm16.__pkgs__,
        description="BLISS project for the F-CRG beamline BM16 (FAME-UHD)",
        long_description=get_readme(),
        license="MIT",
        author="FAME team",
        author_email="fame@esrf.fr",
        url="https://gitlab.esrf.fr/F-CRG/bm16",
        classifiers=[
            "Development Status :: 1 - Planning",
            "License :: OSI Approved :: MIT",
            "Operating System :: OS Independent",
            "Programming Language :: Python :: 3",
            "Topic :: Scientific/Engineering :: Physics",
            "Intended Audience :: Education",
            "Intended Audience :: Science/Research",
        ],
    )

    setup(**kwargs)


if __name__ == "__main__":
    main()
