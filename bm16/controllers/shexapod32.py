# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
Copy of bm32.controllers.shexapod32

Author: Olivier Ulrich

Symetrie hexapod, with extra parameters for origine definition

YAML_ configuration example:

.. code-block:: yaml

    plugin: emotion
    class: SHexapod32
    version: 2          # (1)
    tcp:
      url: id99hexa1
    tx_orig: 0
    ty_orig: 0
    tz_orig: 0
    rx_orig: 0
    ry_orig: 0
    rz_orig: 0
    axes:
      - name: h1tx
        role: tx
        unit: mm
      - name: h1ty
        role: ty
        unit: mm
      - name: h1tz
        role: tz
        unit: mm
      - name: h1rx
        role: rx
        unit: deg
      - name: h1ry
        role: ry
        unit: deg
      - name: h1rz
        role: rz
        unit: deg

1. API version: valid values: 1 or 2 (optional. If no version is given, it
   tries to discover the API version). Authors recommend to put the version
   whenever possible.
"""


from bliss.controllers.motors.shexapod import SHexapod, ROLES, Pose

# from bliss.controllers.motors.shexapod import BaseHexapodError
# from bliss.controllers.motors.shexapod import BaseHexapodProtocol
# from bliss.controllers.motors.shexapod import SHexapod


# from collections import namedtuple
from math import pi

""" Symetrie hexapods work only with mm and deg, but mrad and microns are more useful units """
CUNIT_TO_UNIT = {
    "mrad": pi / 180.0 * 1000,
    "rad": pi / 180.0,
    "micron": 1 / 1000.0,
    "mm": 1,
    "deg": 1,
}


# ROLES = "tx", "ty", "tz", "rx", "ry", "rz"
# Pose = namedtuple("Pose", ROLES)


class SHexapod32(SHexapod):
    """
    Symetrie hexapod controller with extended feature for origin
    definition in yml file.
    Origin is set at startup.
    """

    def __init__(self, *args, **kwargs):
        SHexapod.__init__(self, *args, **kwargs)
        user = self.config.get("user").split(" ")
        object = self.config.get("object").split(" ")
        if (len(user) + len(object)) != 12:
            raise ValueError(
                "Wrong parameter number : need 12 values to define user and oject origin"
            )

        try:
            cmd = (
                "Q80=%f Q81=%f Q82=%f Q83=%f Q84=%f Q85=%f \
Q86=%f Q87=%f Q88=%f Q89=%f Q90=%f Q91=%f Q20=21"
                % (
                    float(user[0]),
                    float(user[1]),
                    float(user[2]),
                    float(user[3]),
                    float(user[4]),
                    float(user[5]),
                    float(object[0]),
                    float(object[1]),
                    float(object[2]),
                    float(object[3]),
                    float(object[4]),
                    float(object[5]),
                )
            )
        except Exception:
            raise TypeError("Need float values to define user and object origin")

        self.protocol().pmac(cmd)

    def info(self):
        print(self.get_info(self))

    def make_ref(self):
        self.protocol()._homing(self)

    def reset(self):
        self.protocol().reset()

    def read_position(self, axis):
        return CUNIT_TO_UNIT[axis.unit] * getattr(
            self.protocol().object_pose, self._SHexapod__get_axis_role(axis)
        )

    def _SHexapod__get_hw_set_positions(self):
        return dict(
            (
                (
                    self._SHexapod__get_axis_role(axis),
                    self._SHexapod__get_hw_set_position(axis)
                    * CUNIT_TO_UNIT[axis.unit],
                )
                for axis in self.axes.values()
            )
        )

    def start_all(self, *motion_list):
        pose_dict = dict(((r, None) for r in ROLES))
        pose_dict.update(self._SHexapod__get_hw_set_positions())
        pose_dict.update(
            dict(
                (
                    (
                        self._SHexapod__get_axis_role(motion.axis),
                        motion.target_pos / CUNIT_TO_UNIT[motion.axis.unit],
                    )
                    for motion in motion_list
                )
            )
        )
        pose = Pose(**pose_dict)
        self.protocol().start_move(pose)
