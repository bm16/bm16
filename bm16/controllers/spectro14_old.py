#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
BLISS controller for BM16 X-ray emission spectrometer
=====================================================

AUTHORS
-------

- [MR] Mauro ROVEZZI <mauro.rovezzi@esrf.fr>


NOTES
-----

"CAS" stands for Crystal Analyser Spectrometer

- Required global variables from config :

  CAS_R : float [1000]
          analyzers bending radius (=diameter Rowland circle)
  CAS_MAT : string ["Si"]
            material of the crystal analyser "Si" or "Ge"
  CAS_HKL : tuple of integers, [(4,4,4)]
            reflection (h,k,l)
  CAS_DZ : float, [82]
           Z offset of analysers row from the central row

  CAS_SZ : float, [500.]
           Z offset of the sample from top of the table (=CAS_samph in confspectro)

REAL MOTORS
-----------

# detector
- xd
- yd
- zd
# analysers table (common to all modules)
- xeq
- zeq
# module 1
- xs1
- xh1
- tilth1
- thetah1
- xb1
- tiltb1
- thetab1
# module 2
- xs2
- xh2
- tilth2
- thetah2
- xb2
- tiltb2
- thetab2
# module 3
- xs3
- xh3
- tilth3
- thetah3
- xb3
- tiltb3
- thetab3
# module 4
- xs4
- xh4
- tilth4
- thetah4
- xb4
- tiltb4
- thetab4
# module 5
- xs5
- xh5
- tilth5
- thetah5
- xb5
- tiltb5
- thetab5
# module 6
- xs6
- xh6
- tilth6
- thetah6
- xb6
- tiltb6
- thetab6
# module 7
- xs7
- xh7
- tilth7
- thetah7
- xb7
- tiltb7
- thetab7

CALCULATED MOTORS
-----------------

# common to all modules
- theta
- emi

"""

# IMPORTS #
import math
from bliss.controllers.motor import CalcController
from bliss.common.utils import object_method

# spectro14
from bm16.utils.bragg import kev2ang, ang2kev, get_dspacing

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d


def rotation_matrix(axis, angle):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by a given angle (degree).
    """
    rangle = np.deg2rad(angle)
    axis = axis / getnorm(axis)
    a = np.cos(rangle / 2.0)
    b, c, d = -axis * np.sin(rangle / 2.0)
    aa, bb, cc, dd = a * a, b * b, c * c, d * d
    bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
    return np.array(
        [
            [aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
            [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
            [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc],
        ]
    )


def getnorm(vector):
    """norm of a vector"""
    return np.sqrt(np.dot(vector, vector))


def normalize(vector):
    """unit vector"""
    n = getnorm(vector)
    if n == 0:
        return np.array([0, 0, 0])
    else:
        return vector / n

def get_angle(v1, v2):
    """angle (degrees) between two vectors"""
    n1 = getnorm(v1)
    n2 = getnorm(v2)
    return np.rad2deg(np.arccos(np.dot(v1, v2) / (n1 * n2)))


class AnalyserD16:
    """Base class for a spherical crystal analyser on a Rowland circle for the BM16 spectrometer (a.k.a spectro14)"""
    
    def __init__(self, theta, pos, row, beta=8, dz=82, R=1000, pos_cen=4, sz=500):
        """constructor
        
        Parameters
        ----------
        
        theta : float
            Bragg angle in degrees
        
        pos : int
            module position possible (e.g. 1,2,3,4,5,6,7)
            the central analyser is given by 'pos_cen' variable (below)
        
        row : string
            "h" or "b" for the top or bottom, respectively
        
        beta : float (optional)
            radial spacing between analysers in degrees [default: 8 (spectro14)]
            
        dz : float
            vertical spacing from the virtual row in mm [default: 82 mm (spectro14)] 
            
        R : float
            diameter of the Rowland circle in mm [default: 1000]
        
        pos_cen : int
            position of the central analyser, that is at beta = 0 [default: 4]
            
        sz : float
            sample offset in Z, that is, sample position is at (0, 0, sz) [default: 500]
            
        Notes
        -----
        
        - [Reference system] Here a global XYZ reference system is used, that is, the ESRF XYZ starndard reference system (right hand, X along the beam) rotated by -90 de counterclockwise around Z
            
        """
        self.pos_cen = pos_cen
        self.pos = pos
        self.row = row
        if self.row == 'h':
            self.dz = dz
        if self.row == 'b':
            self.dz = -1 * dz
        self.sz = sz
        self.theta = theta
        self.beta = beta
        self.R = R
        self._init_position = True        
        
    @property
    def xaxis(self):
        return np.array([1, 0, 0])

    @property
    def yaxis(self):
        return np.array([0, 1, 0])

    @property
    def zaxis(self):
        return np.array([0, 0, 1])
    
    @property
    def theta(self):
        self._theta = 90 - get_angle(self.normal, self.position)
        self._rtheta = np.deg2rad(self._theta)
        return self._theta

    @theta.setter
    def theta(self, value):
        self._theta = value
        self._rtheta = np.deg2rad(self._theta)

    @property
    def beta(self):
        return self._beta

    @beta.setter
    def beta(self, value):
        self._beta = value * (self.pos_cen - self.pos)

    @property
    def R(self):
        return self._R

    @R.setter
    def R(self, value):
        self._R = value

    @property
    def gamma(self):
        """gamma angle for the current analyser"""      
        Rp = self.R * np.sin(self._rtheta)
        gamma_ = np.arcsin(np.cos(self._rtheta) + self.dz / Rp)
        return -(np.rad2deg(gamma_) - 90 + self._theta)

    @property 
    def name(self):
        """name of the crystal in the form of '{row}{position}'"""
        return f"{self.row}{self.pos}"

    @property
    def virt_cen_pos(self):
        """position of the central analyser of the virtual row"""
        return np.array([self.R * np.sin(self._rtheta)**2, 0 , self.R * np.sin(self._rtheta) * np.cos(self._rtheta)])

    @property
    def x0(self):
        return self.virt_cen_pos[0]

    @property
    def y0(self):
        return self.virt_cen_pos[1]

    @property
    def z0(self):
        return self.virt_cen_pos[2] + self.sz
    
    @property
    def position(self):
        """analyser XYZ position"""
        if self._init_position == False:
            return self._pos
        else:
            rz_beta = np.dot(rotation_matrix(self.zaxis, self.beta), self.virt_cen_pos)
            ry_gam = np.dot(rotation_matrix(self.yaxis, self.gamma), rz_beta)
            ry_gam[2] += self.sz
            self._pos = ry_gam
            return self._pos
  
    def init_position(self):
        self._init_position = True
        _ = self.position
        
    @position.setter
    def position(self, value):
        """this setter in case one want to manually change the position of the analyser"""
        self._init_position = False
        assert (isinstance(value, np.ndarray) and len(value)==3), f"{value} should be a numpy array of 3 elements"
        self._pos = value
    
    @property
    def x(self):
        return self.position[0]

    @property
    def y(self):
        return self.position[1]

    @property
    def z(self):
        return self.position[2]
    
    @property
    def normal(self):
        """normal to the analyser"""
        rz_beta = np.dot(rotation_matrix(self.zaxis, self.beta), self.xaxis)
        ry_gam = np.dot(rotation_matrix(self.yaxis, self.gamma), rz_beta)
        return ry_gam
    
    def get_info(self):
        xstr = f"x{self.row}"
        thstr = f"t{self.row}"
        idict = apos = show_spectro_overview(self._theta, retdict=True)
        idict.update({'name' : self.name,
                      'theta' : self._theta,
                      'x' : self.x,
                      'y' : self.y,
                      'z' : self.z,
                      'pitch' : self.pitch,
                      'yaw' : self.yaw,
                      'roll': 0,
                      'zeqdz' : apos['zeq'] + self.dz,
                      'zoff' : self.z - (apos['zeq'] + self.dz),
                      'x0' : self.x0,
                      'xan' : apos[xstr],
                      'xstr' : xstr,
                      'xoff' : self.xs - self.x0 - apos[xstr],
                      'thstr' : thstr,
                      'than' : apos[thstr],
                      'thnm' : -self.gamma,
                })
        istr = ["Analyser {name} @ theta: {theta:.4f} deg"]
        istr.append("X: {x:.4f}, Y: {y:.4f}, Z: {z:.4f}")
        istr.append("pitch: {pitch:.3f}, yaw: {yaw:.3f}, roll: {roll}")
        istr.append("zeq: {zeq:.4f}, zeq+dz: {zeqdz:.4f} (zoff=z-(zeq+dz): {zoff:.4f})")
        istr.append("xs: {xs:.4f} (x0: {x0:.4f})")
        istr.append("{xstr}: {xan:.4f} (xoff=xs-x0-{xstr}: {xoff:.4f})")
        istr.append("{thstr}: {than:.3f} (-gamma: {thnm:.3f})")
        
        istr = '\n'.join(istr).format(**idict)
        print(istr)
   
    @property
    def pitch(self):
        return get_angle(self.normal, self.xaxis)
    
    @property
    def yaw(self):
        return self.beta
    
    @property
    def roll(self):
        raise NotImplementedError

    @property
    def xs(self):
        """xs is the radial distance from the sample along beta"""
        return np.sqrt(self.x**2 + self.y**2)




# DEBUGGING STUFF
DBG_FLAG = 0  # 1=show debug messages; 0=skip debug messages


def print_dbg(pstr):
    if not DBG_FLAG:
        return
    print("DEBUG: {0}".format(pstr))


# UTILITY FUNCTIONS


def calc_det_dzh(theta):
    """Calculate detector vertical offset of the top raw

    .. note:: formula taken from Table 2 on pag. 68 of CE document
              vol. 3 (Olivier Proux et al.)

    """
    return 919.49 - 27.018 * theta + 0.26209 * theta ** 2 - 0.00083803 * theta ** 3


def calc_det_dzb(theta):
    """Calculate detector vertical offset of the bottom raw

    .. note:: formula taken from Table 2 on pag. 68 of CE document
              vol. 3 (Olivier Proux et al.)

    """
    return -677.96 + 19.121 * theta - 0.17315 * theta ** 2 + 0.00049335 * theta ** 3


def calc_det_dz(theta):
    """Calculate detector vertical offset from focus both rows"""
    return abs(calc_det_dzh(theta)) + abs(calc_det_dzb(theta))


def calc_pos_com(emi, d=0, r=0, dz=0, sz=0):
    """Get spectrometer positions for common axes

    Parameters
    ----------
    emi  : emission energy [keV]
    d    : analyser d-spacing [nm]
    r    : crystal bending radius (=diameter Rowland circle) [mm]
    dz   : offset in z from the central row [mm]
    sz   : offset in z of the sample from top table [mm]

    Returns
    -------
    dictionary with real motors positions

    {
     "xs"     : float, # horizontal position central row [mm]
     "zeq"    : float, # vertical position central row   [mm]
     "zd"     : float, # vertical position detector      [mm]
     "xh"     : float, # hor correction top row          [mm]
     "xb"     : float, # hor correction bottom row       [mm]
     "thetah" : float, # theta correction top row        [deg]
     "thetab" : float  # theta correction bottom row     [deg]
    }
    """
    _rtheta = kev2ang(emi, d, deg=False)
    _xs = r * math.sin(_rtheta) * math.sin(_rtheta)
    _zeq = r * math.sin(_rtheta) * math.cos(_rtheta)
    _zd = 2 * _zeq  # detector
    _rsth = r * math.sin(_rtheta)
    try:
        _xh = math.sqrt(_rsth ** 2 - (_zeq + dz) ** 2) - _xs
        _xb = math.sqrt(_rsth ** 2 - (_zeq - dz) ** 2) - _xs
        _thetah = math.degrees(
            math.atan((_zeq + dz) / (_xh + _xs)) + _rtheta - (math.pi / 2.0)
        )
        _thetab = math.degrees(
            math.atan((_zeq - dz) / (_xb + _xs)) + _rtheta - (math.pi / 2.0)
        )
    except Exception:
        _xh = 0.0
        _xb = 0.0
        _thetah = 0.0
        _thetab = 0.0

    _com_dict = {
        "xs": _xs,
        "zeq": _zeq + sz,
        "zd": _zd + sz,
        "xh": _xh,
        "xb": _xb,
        "thetah": _thetah,
        "thetab": _thetab,
    }

    return _com_dict


def calc_pos_mod(nmodule):
    """Get positions per module"""
    raise NotImplementedError("TO IMPLEMENT!")


def show_spectro_overview(theta, d=None, r=1000, dz=82, sz=500.0):
    """show an overview of the spectrometer geometry calculations"""

    if d is not None:
        ene = ang2kev(theta, d)
    else:
        ene = "no dspacing"
    rtheta = math.radians(theta)
    p = r * math.sin(rtheta)
    xs = r * math.sin(rtheta) ** 2
    zeq = r * math.sin(rtheta) * math.cos(rtheta)
    zd = zeq * 2 + sz
    xsh = math.sqrt(p ** 2 - (zeq + dz) ** 2)
    xsb = math.sqrt(p ** 2 - (zeq - dz) ** 2)
    xh = xsh - xs
    xb = xsb - xs
    rth = math.acos(xsh / p) + rtheta - math.pi / 2.0
    rtb = math.acos(xsb / p) + rtheta - math.pi / 2.0
    th = math.degrees(rth)
    tb = math.degrees(rtb)

    xdb = 2 * zeq * math.sin(abs(rtb))
    zdb = xdb / math.tan(rtheta + rtb)

    print(
        "theta = {0} (ene = {3}), r = {1}, dz = {2}, sz = {4}".format(
            theta, r, dz, ene, sz
        )
    )
    print("p = {0}".format(p))
    print("xs = {0}".format(xs))
    print("zeq = {0}".format(zeq + sz))
    print("zd = {0}".format(zd))
    print("=== top row ===")
    print("xh = {0}".format(xh))
    print("th = {0}".format(th))
    print("=== bottom row ===")
    print("xb = {0}".format(xb))
    print("tb = {0}".format(tb))
    print("xdb = {0}".format(xdb))
    print("zdb = {0}".format(zdb))


# CLASS #
class Spectro14(CalcController):
    def __init__(self, *args, **kwargs):
        """controller init"""
        CalcController.__init__(self, *args, **kwargs)
        self._pos_dict = {}
        self._conf_dict = {}
        self.CAS_MAT = self.config.get("CAS_MAT", default="Si")
        self.CAS_HKL = self.config.get("CAS_HKL", default=(4, 4, 4))
        self.CAS_R = self.config.get("CAS_R", float, default=1000.0)
        self.CAS_DZ = self.config.get("CAS_DZ", float, default=82.0)
        self.CAS_SZ = self.config.get("CAS_SZ", float, default=500.0)
        self.CAS_THmin = self.config.get("CAS_THmin", float, default=65.0)
        self.CAS_THmax = self.config.get("CAS_THmax", float, default=89.0)
        self.CAS_d = get_dspacing(self.CAS_MAT, self.CAS_HKL)
        self.update_conf()

    def update_conf(self):
        """update configuration dictionary"""
        self._conf_dict.update(
            {
                "CAS_R": self.CAS_R,
                "CAS_MAT": self.CAS_MAT,
                "CAS_HKL": self.CAS_HKL,
                "CAS_DZ": self.CAS_DZ,
                "CAS_SZ": self.CAS_SZ,
                "CAS_THmin": self.CAS_THmin,
                "CAS_THmax": self.CAS_THmax,
                "CAS_d": self.CAS_d,
            }
        )

    def initialize_axis(self, axis):
        CalcController.initialize_axis(self, axis)
        axis.no_offset = True

    def _check_theta(self, _emi):
        """Check if the given energy is within theta range"""
        _theta = kev2ang(_emi, self.CAS_d, deg=True)
        if (_theta >= self.CAS_THmax) or (_theta <= self.CAS_THmin):
            print(
                "ERROR: requested energy ({0:.5f} keV) is not reacheable!".format(_emi)
            )
            return False
        else:
            return True

    # BLISS METHODS
    def calc_from_real(self, positions_dict):
        """get real motors positions positions
        (INFO: the method is called each time a real motor is moved)

        TODO
        ----

        - for a given 'emi' this method should only check if any real
        motor is out of the calculated position. If it is the case, it
        should raise a warning only.

        """
        print_dbg("entering calc_from_real")
        print_dbg("received positions:\n {0}".format(positions_dict))
        self._pos_dict.update(positions_dict)

        _zeq = positions_dict["zeq"] - self.CAS_SZ
        _xs = positions_dict["xs"]

        try:
            _rtheta = math.atan(_xs / _zeq)
        except Exception:
            print("ERROR: impossible to calculate theta, using 0")
            _rtheta = 0.0

        _theta = math.degrees(_rtheta)
        _emi = ang2kev(_rtheta, self.CAS_d, deg=False)

        # if not (_zd == 2*_zeq):
        #    print("WARNING: inconsistent position for zd and zeq")
        # commented because otherwise shows up at any mouvement

        _calc_dict = {"theta": _theta, "emi": _emi}

        print_dbg("\n all motors positions\n {0}".format(positions_dict))
        _confs = [self.CAS_R, self.CAS_MAT, self.CAS_HKL, self.CAS_d, self.CAS_SZ]
        print_dbg("\n all configs\n {0}".format(_confs))
        print_dbg("\n all calculated positions\n {0}".format(_calc_dict))

        self._pos_dict.update(_calc_dict)
        # return real positions_dict
        return _calc_dict

    def calc_to_real(self, positions_dict):
        """
        (INFO: the method is called each time a real motor is moved)
        """
        print_dbg("entering calc_to_real")
        _emi0 = self.pseudos[0].position()  # _pos_dict["emi"] #energy before movement
        _emi = positions_dict["emi"]  # requested new energy

        if not self._check_theta(_emi):
            _emi = _emi0

        _real_dict = calc_pos_com(
            _emi, d=self.CAS_d, r=self.CAS_R, dz=self.CAS_DZ, sz=self.CAS_SZ
        )
        self._pos_dict.update(_real_dict)
        # return real positions_dict
        return _real_dict

    # INFO: in-axis method accepts/returns only one parameter defined in the decorator
    @object_method(types_info=(None, "dict"))
    def get_positions(self, axis):
        """get a dictionary with all relevant current positions

        Parameters
        ==========
        None

        Return
        ======
        _curr_dict : dictionary with all pseudo and motor positions


        .. note:: in-axis utility method
        """
        print_dbg("axis.name: {0}".format(axis.name))
        if not axis.name == "emi":
            print("ERROR: this utily method works only on 'emi' axis")
            return 0
        _curr_dict = {}
        for pseudo in self.pseudos:
            print_dbg("{0} : {1}".format(pseudo.name, pseudo.position()))
            _curr_dict.update({pseudo.name: pseudo.position()})
        for real in self.reals:
            print_dbg("{0} : {1}".format(real.name, real.position()))
            _curr_dict.update({real.name: real.position()})
        return _curr_dict

    @object_method(types_info=(None, "dict"))
    def get_conf(self, axis):
        """get a dictionary with current configuration

        Parameters
        ==========
        None

        Return
        ======
        _conf_dict : dictionary with current configuration


        .. note:: in-axis utility method
        """
        print_dbg("axis.name: {0}".format(axis.name))
        if not axis.name == "emi":
            print("ERROR: this utily method works only on 'emi' axis")
            return 0
        try:
            return axis.controller._conf_dict
        except Exception:
            print("ERROR: axis.controller._conf_dict not found")
            return {}

    @object_method(types_info=("float", None))
    def calc_positions(self, axis, newemi):
        """show current and calculated new positions for a given energy

        Parameters
        ==========
        newemi : emission energy in keV to get the positions

        Return
        ======
        No returns. Prints to screen current and new positions.

        .. note:: in-axis utility method
        """
        _curr_dict = self.get_positions(self.pseudos[0])
        _new_dict = calc_pos_com(
            newemi, d=self.CAS_d, r=self.CAS_R, dz=self.CAS_DZ, sz=self.CAS_SZ
        )
        _new_dict.update(
            {"emi": newemi, "theta": kev2ang(newemi, self.CAS_d, deg=True)}
        )
        _tmpl_head = "|{0:^10}|{1:^10}|{2:^10}|"
        _tmpl_line = "|{0:^10}|{1:^10.5f}|{2:^10.5f}|"
        _tmpl_spcr = "----------"
        print("INFO: calculated positions for {0} keV".format(newemi))
        print(_tmpl_head.format("AXIS", "CURR", "NEW"))
        print(_tmpl_head.format(_tmpl_spcr, _tmpl_spcr, _tmpl_spcr))
        print(_tmpl_line.format("emi", _curr_dict["emi"], _new_dict["emi"]))
        print(_tmpl_line.format("theta", _curr_dict["theta"], _new_dict["theta"]))
        print(_tmpl_line.format("zeq", _curr_dict["zeq"], _new_dict["zeq"]))
        print(_tmpl_line.format("zd", _curr_dict["zd"], _new_dict["zd"]))
        print(_tmpl_line.format("xs", _curr_dict["xs"], _new_dict["xs"]))
        print(_tmpl_line.format("xb", _curr_dict["xb"], _new_dict["xb"]))
        print(_tmpl_line.format("xh", _curr_dict["xh"], _new_dict["xh"]))
        print(_tmpl_line.format("thetab", _curr_dict["thetab"], _new_dict["thetab"]))
        print(_tmpl_line.format("thetah", _curr_dict["thetah"], _new_dict["thetah"]))


# FOR TESTS #
if __name__ == "__main__":
    # show_spectro_overview(65)
    theta_bragg = 75
    t = AnalyserD16(theta_bragg, 5, "h")
    t.get_info()
    pass
