# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2019 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
adapted from bm32 dsp7225
dsp7225 controller
yml configuration example:
channel_name : dac1, dac2, oscf, osca,mag,sat


package: bm32.controllers.dsp7225
class: dsp7225
controller:
serial:
   url: "/dev/ttyUSB0"
   baudrate: 2400
axes:
    - name: dac
      channel_name: dac2
      steps_per_unit: 1
      velocity: 1
      acceleration: 1
      unit: "V"

    - name: freq
      channel_name: oscf
      steps_per_unit: 1
      velocity: 1
      acceleration: 1
      unit: "Hz"

counters:
    - counter_name: mag
      channel_name: mag
    - counter_name: pha
      channel_name: pha
    - counter_name: sat
      channel_name: sat


default: "OF. 305.0; OA. 0.002; CP O; FET 1; AGAIN 0;
          FLOAT 0; FNF 1; IE 0; LF 0; IMODE 1; SEN 21
          SLOPE 3; TC 11; REFP. 0.0 "
"""

#import warnings
#import serial
#import logging
import time
#from struct import *
#import enum

#from bliss.common.utils import object_method
#from bliss.comm.serial import Serial
from bliss.comm.util import get_comm, TCP  #, SERIAL
from bliss.controllers.motor import Controller
from bliss.common.axis import AxisState

from bliss.common.counter import SamplingCounter, SamplingMode
from bliss.controllers.counter import SamplingCounterController

from bliss.common.logtools import log_debug, log_info, log_error


# not all commands listed ... only useful for us (different from dsp7225!)
DSP7230CMD = {
    "IMODE": {
        0: "Current mode off, voltage mode enabled",
        1: "High bandwidth (HB) current mode enabled - connect signal on B input",
        2: "Low noise (LN) current mode enabled - connect signal to B input",
    },
    "NOISEMODE": {
        0: "Noise measurement mode off",
        1: "Noise measurement mode on",
    },
    "FET": {
        0: "input device used at the signal input: Bipolar",
        1: "input device used at the signal input: FET",
    },
    "FLOAT": {
        0: "Grounded",
        1: "Float",
    },
    "DCCOUPLE": {
        0: "AC",
        1: "DC",
    },
    "SEN": {  # depending on IMODE (0, 1, 2)
        3: ("10nV", "10fA", "n/a"),
        4: ("20nV", "20fA", "n/a"),
        5: ("50nV", "50fA", "n/a"),
        6: ("100nV", "100fA", "n/a"),
        7: ("200nV", "200fA", "2fA"),
        8: ("500nV", "500fA", "5fA"),
        9: ("1µV", "1pA", "10fA"),
        10: ("2µV", "2pA", "20fA"),
        11: ("5µV", "5pA", "50fA"),
        12: ("10µV", "10pA", "100fA"),
        13: ("20µV", "20pA", "200fA"),
        14: ("50µV", "50pA", "500fA"),
        15: ("100µV", "100pA", "1pA"),
        16: ("200µV", "200pA", "2pA"),
        17: ("500µV", "500pA", "5pA"),
        18: ("1mV", "1nA", "10pA"),
        19: ("2mV", "2nA", "20pA"),
        20: ("5mV", "5nA", "50pA"),
        21: ("10mV", "10nA", "100pA"),
        22: ("20mV", "20nA", "200pA"),
        23: ("50mV", "50nA", "500pA"),
        24: ("100mV", "100nA", "1nA"),
        25: ("200mV", "200nA", "2nA"),
        26: ("500mV", "500nA", "5nA"),
        27: ("1V", "1µA", "10nA"),
    },
    "ACGAIN": {
        0: "0dB",
        1: "6dB",
        2: "12dB",
        3: "18dB",
        4: "24dB",
        5: "30dB",
        6: "36dB",
        7: "42dB",
        8: "48dB",
        9: "54dB",
        10: "60dB",
        11: "66dB",
        12: "72dB",
        13: "78dB",
        14: "84dB",
        15: "90dB",
    },
    "LF": {
        "0,0": "line frequency rejection Off",
        "0,1": "line frequency rejection Off",
        "1,0": "line frequency rejection on 60 Hz",
        "1,1": "line frequency rejection on 50 Hz",
        "2,0": "line frequency rejection on 120 Hz",
        "2,1": "line frequency rejection on 100 Hz",
        "3,0": "line frequency rejection on 60 and 120 Hz",
        "3,1": "line frequency rejection on 50 and 100 Hz",
    },
    "IE": {
        0: "Internal reference",
        1: "External reference TTL from front panel REF input",
        2: "External reference analog from front panel REF input",
    },
    "SLOPE": {
        0: "6dB/octave",
        1: "12dB/octave",
        2: "18dB/octave",
        3: "24dB/octave",
    },
    "FASTMODE": {
        0: "Output Filter Mode Control: Fast mode off",
        1: "Output Filter Mode Control: Fast mode on",
    },
    "DEMOD2SRC": {
        0: "Main signal channel ADC (i.e. both demodulators are fed the same signal)",
        1: "ADC1 - rear panel auxiliary ADC input",
        2: "Demodulator 1 X channel output - this is the Tandem demodulation mode",
    },
    "SYNC": {
        0: "Synchronous time constant disabled",
        1: "Synchronous time constant enabled",
    },
    "REFMODE": {
        0: "Single Reference / Virtual Reference mode",
        1: "Dual Harmonic mode",
        2: "Dual Reference mode",
    },
    "TC": {  # depending on NOISEMODE (0, 1)
        0: ("10µs", "n/a"),
        1: ("20µs", "n/a"),
        2: ("50µs", "n/a"),
        3: ("100µs", "n/a"),
        4: ("200µs", "n/a"),
        5: ("500µs", "500µs"),
        6: ("1ms", "1ms"),
        7: ("2ms", "2ms"),
        8: ("5ms", "5ms"),
        9: ("10ms", "10ms"),
        10: ("20ms", "n/a"),
        11: ("50ms", "n/a"),
        12: ("100ms", "n/a"),
        13: ("200ms", "n/a"),
        14: ("500ms", "n/a"),
        15: ("1s", "n/a"),
        16: ("2s", "n/a"),
        17: ("5s", "n/a"),
        18: ("10s", "n/a"),
        19: ("20s", "n/a"),
        20: ("50s", "n/a"),
        21: ("100s", "n/a"),
        22: ("200s", "n/a"),
        23: ("500s", "n/a"),
        24: ("1ks", "n/a"),
        25: ("2ks", "n/a"),
        26: ("5ks", "n/a"),
        27: ("10ks", "n/a"),
        28: ("20ks", "n/a"),
        29: ("50ks", "n/a"),
        30: ("100ks", "n/a"),
    },
}

DSPCHANNELMAP = {
    "DAC1": "DAC. 1",
    "DAC2": "DAC. 2",
    "OSCF": "OF. ",
    "OSCA": "OA. ",
    "MAG": "X.",
    "PHA": "PHA.",
    "SAT": "ST.",
}

FOSC_HLIM = 500  # Hz
FOSC_LLIM = 0.001  # Hz

AOSC_HLIM = 0.2  # V
AOSC_LLIM = 0


class Dsp7230CounterController(SamplingCounterController):
    def __init__(self, name, controller):
        super().__init__(name)
        self.counter_channel = {}
        self.controller = controller
        self.value = dict()
        self.last_value = dict()
        for counter_conf in controller.config.config_dict["counters"]:
            cname = counter_conf["counter_name"]
            channel = counter_conf["channel_name"].upper()
            # print("%s %s:%s" % (name, cname, channel))
            counter = self.create_counter(
                SamplingCounter, cname, mode=SamplingMode.SINGLE
            )
            self.counter_channel[counter.name] = DSPCHANNELMAP[channel]

    def read(self, counter):
        v = self.controller.io_read(self.counter_channel[counter.name])
        try:
            self.value[counter.name] = float(v)
        except Exception:
            pass

        if counter.name == 'mag':
            return self.value[counter.name] * 1e12
        else:
            return self.value[counter.name]

    #def read(self, counter):
    #    try:
    #        v = self.controller.io_read(self.counter_channel[counter.name])
    #    except Exception:
    #        pass
    #    try:
    #        self.value[counter.name] = float(v)
    #    except Exception:
    #        self.value[counter.name] = self.last_value[counter.name]
    #    self.last_value[counter.name] = self.value[counter.name]
    #    return self.value[counter.name]

    def read_all(self, *counters):
        rvalues = list()
        for i in counters:
            rvalues.append(self.read(i))
        return rvalues


class Dsp7230(Controller):
    def __init__(self, *args, **kwargs):
        Controller.__init__(self, *args, **kwargs)
        self._channel = {}
        self.counter_channel = {}
        for i in self.config.config_dict["axes"]:
            try:
                self._channel[i["name"]] = DSPCHANNELMAP[i["channel_name"].upper()]
            except KeyError:
                log_error(self, "Wrong channel name : %s" % i["channel_name"])

        try:
            self.comm = get_comm(self.config.config_dict, ctype=TCP)
            self.comm._eol = "\r"
            # self.comm = get_comm(
            #     self.config.config_dict,
            #     SERIAL,
            #     baudrate=self.config.config_dict["serial"]["baudrate"],
            #     bytesize=serial.SEVENBITS,
            #     parity=serial.PARITY_EVEN,
            #     stopbits=serial.STOPBITS_ONE,
            #     rtscts=False,
            #     xonxoff=False,
            #     eol=b"\r",
            #     timeout=1,
            # )
        except ValueError:
            raise RuntimeError("No serial field")
        else:
            pass
        # Creation of a scale factor to obtain the MAG value at the good scale:
        self._mag_scale = 1
        self._mag_unit = "unknown"
        # commemted for the tests because too long
        log_info(self, "Initializing module DSP7230, please wait ...")
        for cmd in self.config.config_dict["default"].split(";"):
            self.io_write(cmd.replace(" ", "", 1))
            print(cmd.replace(" ", "", 1))
            time.sleep(0.1)
            if cmd.split(" ")[0] == "IMODE":
                _imode = cmd.split(" ")[1]
                if _imode == "0":
                    self._mag_scale = 1000  # the obtained value is in mV
                    self._mag_unit = "mV"
                if _imode == "1":
                    self._mag_scale = 1e9  # the obtained value is in nA
                    self._mag_unit = "nA"
                if _imode == "2":
                    self._mag_scale = 1e11  # the obtained value is in 1/100nA
                    self._mag_unit = "1/100nA"

        self.cntctrl = Dsp7230CounterController("DspCounters", self)

    def initialize(self):
        log_debug(self, "initialize")

    def exit(self):
        log_debug(self, "exit")
        self.comm.close()

    def finalize(self):
        log_debug(self, "finalize")
        self.comm.close()

    # Initialize each axis.
    def initialize_axis(self, axis):
        log_debug(self, "initialize_axis")

    def read_position(self, axis):
        log_debug(self, "read_position")
        reply = self.io_read(self._channel[axis.name])
        log_debug(self, "Dsp7230: read position %r" % reply.strip())
        return float(reply.strip())

    def read_acceleration(self, axis):
        log_debug(self, "read_acceleration")
        return 1

    def read_deceleration(self, axis):
        log_debug(self, "read_deceleration")
        return 1

    def read_velocity(self, axis):
        log_debug(self, "read_velocity")
        return 1

    def read_firstvelocity(self, axis):
        self.log.debug("read_firstvelocity")
        return 1

    def set_velocity(self, axis, velocity):
        log_debug(self, "set_velocity")
        pass

    def set_acceleration(self, axis, acceleration):
        log_debug(self, "set_acceleration")
        pass

    def state(self, axis):
        log_debug(self, "state")
        return AxisState("READY")

    def start_one(self, motion):
        log_debug(self, "start_one %r " % motion.target_pos)
        self.io_write(self._channel[motion.axis.name], motion.target_pos)
        if motion.axis.name in ["freq", "dac"]:
            time.sleep(0.2)

    def start_all(self, *motion_list):
        log_debug(self, "start_all")
        for motion in motion_list:
            self.start_one(motion)

    def __info__(self):
        return self.get_info()

    def get_info(self):
        log_debug(self, "get_info")
        info = list()
        for i in DSP7230CMD:
            try:
                v = int(self.io_read(i))
            except Exception:
                pass
            if i == "IMODE":
                imode = v
            if i == "NOISEMODE":
                noisemode = v
            if i == "SEN":
                info.append("{:10s}{:2d}:   {:s}".format(i, v, DSP7230CMD[i][v][imode]))
            if i == "TC":
                info.append(
                    "{:10s}{:2d}:   {:s}".format(i, v, DSP7230CMD[i][v][noisemode])
                )
            if i == "LF":
                v = str(self.io_read(i))
                v = v[:-1]  # to suppress \n
                info.append("{:10s}{:s}:   {:s}".format(i, v, DSP7230CMD[i][v]))
            if i != "SEN" and i != "TC" and i != "LF":
                info.append("{:10s}{:2d}:   {:s}".format(i, v, DSP7230CMD[i][v]))
        info.append(
            "F={:.3f} Hz @ {:.0f} mV".format(
                float(self.io_read(DSPCHANNELMAP["OSCF"])),
                float(self.io_read(DSPCHANNELMAP["OSCA"])) * 1000,
            )
        )
        return "\n".join(info)

    def stop_one(self, motion):
        log_debug(self, "stop")

    def stop_all(self, *motion_list):
        log_debug(self, "stop_all")
        for motion in motion_list:
            self.stop_one(motion)

    def io_write(self, cmd, val=None):
        log_debug(self, "io_write reg=%r val=%r" % (cmd, val))

        if val is not None:
            if cmd in DSP7230CMD:
                cmd = cmd + " " + "{}".format(val)
            else:
                cmd = cmd + " " + "{:.3f}".format(val)

        cmd = cmd + "\r"
        self.comm.write(cmd.encode())
        log_debug(self, "   write :%r" % cmd)
        return

    def io_read(self, cmd, val=None):
        log_debug(self, "io_read reg=%r val=%r" % (cmd, val))

        if val is not None:
            cmd = cmd + str(val)
        cmd = cmd + "\r"
        log_debug(self, "   write :%r" % cmd)
        with self.comm._lock:
            self.comm.flush()
            ans = self.comm.write_readline(
                cmd.encode(), eol="\0"
            ).decode()  # the null character is necessary in tcp mode.
        if len(ans) > 1:
            if ord(ans[-1]) == 0:
                ans = ans[0:-1]
            if "X." in cmd:
                ans = float(ans) * self._mag_scale
            ans = str(ans)
            log_debug(self, "   read   :%r" % ans)
        return ans

    def get_formated_info(self):
        formated_info = {}
        formated_info["DAC"] = "{}".format(float(self.io_read("DAC. 1")))
        formated_info["Fosc"] = "{}".format(float(self.io_read("OF.")))
        formated_info["Aosc"] = "{}".format(float(self.io_read("OA.")))
        noisemode = int(self.io_read("NOISEMODE"))
        v = int(self.io_read("TC"))
        formated_info["TimeC"] = "{}".format(DSP7230CMD["TC"][v][noisemode])
        imode = int(self.io_read("IMODE"))
        v = int(self.io_read("SEN"))
        formated_info["Sens"] = "{}".format(DSP7230CMD["SEN"][v][imode])
        return formated_info

    def set_Fosc(self, freq_Hz):
        if FOSC_LLIM <= freq_Hz <= FOSC_HLIM:
            self.io_write(DSPCHANNELMAP["OSCF"], freq_Hz)
        else:
            log_error(self, "oscillation frequency must be in the range {}-{} Hz".format(FOSC_LLIM, FOSC_HLIM))

    def set_Aosc(self, amp_V):
        if AOSC_LLIM <= amp_V <= AOSC_HLIM:
            self.io_write(DSPCHANNELMAP["OSCA"], amp_V)
        else:
            log_error(self, "oscillation amplitude must be in the range {}-{} V.".format(AOSC_LLIM, AOSC_HLIM))

    def increase_Sens(self):
        val = int(self.io_read("SEN")) + 1
        self.io_write("SEN " + str(val))
        time.sleep(0.2)
        print("Sens: {}".format(self.get_formated_info()["Sens"]))

    def decrease_Sens(self):
        val = int(self.io_read("SEN")) - 1
        self.io_write("SEN " + str(val))
        time.sleep(0.2)
        print("Sens: {}".format(self.get_formated_info()["Sens"]))

    def increase_TimeC(self):
        val = int(self.io_read("TC")) + 1
        self.io_write("TC " + str(val))
        time.sleep(0.2)
        print("TimeC: {}".format(self.get_formated_info()["TimeC"]))

    def decrease_TimeC(self):
        val = int(self.io_read("TC")) - 1
        self.io_write("TC " + str(val))
        time.sleep(0.2)
        print("TimeC: {}".format(self.get_formated_info()["TimeC"]))
