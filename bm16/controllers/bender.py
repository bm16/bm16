#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
from types import SimpleNamespace
from bliss.controllers.motor import CalcController


class PolynomialBender:
    def __init__(self, name, config):
        self.name = name
        self.config = config
        self._offset = self.config.get("offset")
        self._p2l = SimpleNamespace(**self.config.get("poly_to_len").to_dict())
        self._p2a = SimpleNamespace(**self.config.get("poly_to_ang").to_dict())

    def to_len(self, bender):
        bender -= self._offset
        actuator = (
            self._p2l.M0
            + self._p2l.M1 * bender
            + self._p2l.M2 * np.power(bender, 2)
            + self._p2l.M3 * np.power(bender, 3)
            + self._p2l.M4 * np.power(bender, 4)
            + self._p2l.M5 * np.power(bender, 5)
        )

        return -1*actuator

    def to_ang(self, actuator):
        actuator *= -1
        bender = (
            self._p2a.M0
            + self._p2a.M1 * actuator
            + self._p2a.M2 * np.power(actuator, 2)
            + self._p2a.M3 * np.power(actuator, 3)
            + self._p2a.M4 * np.power(actuator, 4)
            + self._p2a.M5 * np.power(actuator, 5)
        )
        bender += self._offset
        return bender


class BenderRotation(CalcController):
    """Calculation controller for the benders based on the weak-points design"""

    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)
        self._poly = self.config.get("polynomial", None)

    def __info__(self):
        info_str = f"CONTROLLER: {self.__class__.__name__}\n"

        return info_str

    def calc_from_real(self, reals_dict):

        pseudos_dict = {}
        pseudos_dict["bender"] = self._poly.to_ang(
            reals_dict["actuator"]
        )
        return pseudos_dict

    def calc_to_real(self, pseudos_dict):

        reals_dict = {}
        reals_dict["actuator"] = self._poly.to_len(
            pseudos_dict["bender"]
        )

        return reals_dict


class BenderMonoFocus(CalcController):
    """Monochromator focalisation bender"""

    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)

    def calc_from_real(self, reals_dict):

        pseudos_dict = {}
        pseudos_dict["focal"] = (reals_dict["actuator1"] + reals_dict["actuator2"] )/2.
        return pseudos_dict

    def calc_to_real(self, pseudos_dict):

        reals_dict = {}
        reals_dict["actuator1"] = pseudos_dict["focal"]
        reals_dict["actuator2"] = pseudos_dict["focal"]
        return reals_dict
