# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import numpy

from bliss.common.utils import autocomplete_property
from bliss.controllers.spectrometers.spectro_base import (
    CylindricalAnalyser,
    Detector,
    Spectrometer,
)
from bliss.controllers.spectrometers.spectro_base import (
    get_angle,
    getnorm,
    direction_to_angles,
    rotation_matrix,
    normalize,
    IncrementalTable,
)


class AnalyserBM16(CylindricalAnalyser):

    def _load_config(self):
        super()._load_config()
        self._vertical_offset = self.config["vertical_offset"]

    def _load_settings(self):
        """Get from redis the persistent spectrometer parameters (redis access)"""

        cached = {}
        cached["miscut"] = self._settings.get("miscut", self.config["miscut"])
        cached["radius"] = self._settings.get("radius", self.config["radius"])
        cached["offset_on_detector"] = self._settings.get(
            "offset_on_detector", self.config["offset_on_detector"]
        )
        cached["bragg_solution"] = self._settings.get("bragg_solution", None)
        cached["referential_origin"] = self._settings.get(
            "referential_origin", [0, 0, 0]
        )

        self._cached_settings = cached

    def _positioner_to_lab_matrix(self, position):
        """Positioner referential [ex,ey,ez] expressed in laboratory referential [X,Y,Z].
        The cylindrical geometry assumes that ex is pointing to the laboratory origin.
        """

        # ==== positioner ref pointing to lab center in <X,Y> plan (Z excluded)
        incident = position * -1
        ex = normalize(numpy.array([incident[0], incident[1], 0]))
        ez = numpy.array([0, 0, 1])
        ey = numpy.cross(ez, ex)
        ez = numpy.cross(ex, ey)
        return numpy.vstack((ex, ey, ez)).T

    def _get_pos(self, tag):
        """return one of the position coordinates [xpos, ypos, zpos] or
        orientation angles (pitch, yaw) expressed in the laboratory referential
        (i.e taking into account a possible spectrometer origin != [0,0,0]).
        The returned value must be computed from the actual real axes position to reflect
        actual positioner situation.
        args: tag is one of ["xpos", "ypos", "zpos", pitch, "yaw"]
        """
        # === !!! motor position is expressed in lab ref (so it already includes referential_origin offset) !!!

        xs = self.config["xs"].position
        zeq = self.config["zeq"].position
        R = self.real_axes["xcorr"].position + xs

        if tag == "xpos":
            ang = numpy.deg2rad(self.angular_offset)
            return R * numpy.cos(ang)
        elif tag == "ypos":
            ang = numpy.deg2rad(self.angular_offset)
            return R * numpy.sin(ang)
        elif tag == "rpos":
            return R
        elif tag == "zpos":
            return zeq + self.vertical_offset
        elif tag == "pitch":
            return self.real_axes["theta"].position
        elif tag == "yaw":
            return 0

        raise RuntimeError(f"unknown tag {tag}")

    @property
    def vertical_offset(self):
        return self._vertical_offset

    @vertical_offset.setter
    def vertical_offset(self, value):
        self._vertical_offset = value
        self._update()

    def compute_bragg_solution(self, bragg):

        radbragg = numpy.deg2rad(bragg)
        drowland = 2 * self.radius

        # === position of the central analyser of the virtual row
        C0 = numpy.array(
            [
                drowland * numpy.sin(radbragg) ** 2,
                0,
                drowland * numpy.sin(radbragg) * numpy.cos(radbragg),
            ]
        )

        # === gamma angle for the current analyser
        Rp = drowland * numpy.sin(radbragg)
        gamma = numpy.arcsin(numpy.cos(radbragg) + self.vertical_offset / Rp)
        gamma = numpy.rad2deg(-(gamma - numpy.pi / 2 + radbragg))

        # === analyser XYZ position
        Y = numpy.array([0, 1, 0])
        Z = numpy.array([0, 0, 1])
        rz_beta = numpy.dot(rotation_matrix(Z, self.angular_offset), C0)
        Ai = numpy.dot(rotation_matrix(Y, gamma), rz_beta)

        xcorr = numpy.sqrt(Ai[0] ** 2 + Ai[1] ** 2) - C0[0]

        reals_pos = {
            "xcorr": xcorr,
            "theta": -gamma,
        }

        bragg_solution = {
            "C0": C0,
            "Ai": Ai,
        }

        return (bragg, bragg_solution, reals_pos)

    def scan_metadata(self):
        meta_dict = {"@NX_class": "NXcollection"}
        meta_dict["crystal"] = str(self.xtal_sel)
        meta_dict["miscut"] = self.miscut
        meta_dict["radius"] = self.radius
        meta_dict["referential_origin"] = self.referential_origin
        meta_dict["offset_on_detector"] = self.offset_on_detector
        return meta_dict


class DetectorBM16(Detector):
    def __init__(self, config):
        super().__init__(config)

    def _get_pos(self, tag):
        """return one of the position coordinates [xpos, ypos, zpos] or
        orientation angles (pitch, yaw) expressed in the laboratory referential
        (i.e taking into account a possible spectrometer origin != [0,0,0]).
        The returned value must be computed from the actual real axes position to reflect
        actual positioner situation.
        args: tag is one of ["xpos", "ypos", "zpos", pitch, "yaw"]
        """
        # === !!! motor position is expressed in lab ref (so it already includes referential_origin offset) !!!
        xpos = self.config.get("xpos", 0) + self.referential_origin[0]
        ypos = self.config.get("ypos", 0) + self.referential_origin[1]
        yaw = self.config.get("yaw", 0)
        pitch = self.config.get("pitch", 0)
        if tag == "rpos":
            x = self._get_pos("xpos")
            y = self._get_pos("ypos")
            return numpy.sqrt(x**2 + y**2)
        elif tag == "ypos":
            return ypos
        elif tag == "yaw":
            return yaw
        elif tag == "xpos":
            return xpos
        elif tag == "zpos":
            return self.real_axes["zpos"].position
        elif tag == "pitch":
            return pitch
        raise RuntimeError(f"unknown tag {tag}")

    def _load_config(self):
        super()._load_config()

    def _load_settings(self):
        """Get from redis the persistent spectrometer parameters (redis access)"""

        cached = {}
        cached["bragg_solution"] = self._settings.get("bragg_solution", None)
        cached["referential_origin"] = self._settings.get(
            "referential_origin", [0, 0, 0]
        )

        self._cached_settings = cached

    @Detector.target.setter
    def target(self, value):
        if not isinstance(value, AnalyserBM16):
            raise ValueError(f"target {value} is not an AnalyserBM16 object")
        self._target = value
        self._update()

    def compute_bragg_solution(self, bragg):
        """returns a tuple (bragg, bragg_solution, reals_positions):
        - bragg: the bragg angle associated to this solution.
        - bragg_solution: a dict with relevant data for the position and orientation of this positioner.
          Data expressed in the laboratory referential with an origin at (0,0,0) (i.e. ignoring self.referential_origin).
        - reals_positions: the theoritical positions of the real axes for the given bragg value.
          Reals positions expressed in laboratory referential (must take into account self.referential_origin!=(0,0,0))
        """
        bsolution = self.target.compute_bragg_solution(bragg)[1]
        C0 = bsolution["C0"]
        xs = C0[0]
        zeq = C0[2] + self.referential_origin[2]
        zpos = 2 * C0[2] + self.referential_origin[2]

        reals_pos = {}
        reals_pos["zpos"] = zpos
        reals_pos["xs"] = xs
        reals_pos["zeq"] = zeq

        return (
            bragg,
            {},
            reals_pos,
        )

    def scan_metadata(self):
        meta_dict = {"@NX_class": "NXcollection"}
        meta_dict["target"] = self.target.name
        return meta_dict


class SpectrometerBM16(Spectrometer):

    def __info__(self):
        estate, bstate = self.is_aligned
        state2txt = {True: "ALIGNED", False: "NOT ALIGNED !!!"}
        aligned = f"ENERGY {state2txt[estate]}"
        aligned += f" ({self.energy_axis.position:.4f} KeV)"
        aligned += f" | BRAGG {state2txt[bstate]}"
        aligned += f" ({self.bragg_axis.position:.4f} deg)"

        title = f" Spectrometer ({self.name}): {aligned} "
        line = "=" * len(title)
        txt = "\n".join(["\n", line, title, line, "\n\n"])

        txt += f"* ITEMS POSITIONS IN LABORATORY REFERENTIAL (spectro origin = {self.referential_origin}):\n\n"
        head = [
            "name",
            "energy",
            "bragg",
            "rpos",
            "xpos",
            "ypos",
            "zpos",
            "pitch",
            "yaw",
        ]
        tab = IncrementalTable([head], col_sep="|", flag="", lmargin="  ")
        for ana in self._active_analysers:
            values = [
                ana.name,
                ana.geo_energy,
                ana.geo_bragg,
                ana.rpos,
                ana.xpos,
                ana.ypos,
                ana.zpos,
                ana.pitch,
                ana.yaw,
            ]
            tab.add_line(values)
        values = [
            self.detector.name,
            self.detector.energy_axis.position,
            self.detector.bragg_axis.position,
            self.detector.rpos,
            self.detector.xpos,
            self.detector.ypos,
            self.detector.zpos,
            self.detector.pitch,
            self.detector.yaw,
        ]
        tab.add_line(values)
        tab.resize(10, 16)
        tab.add_separator("-", line_index=1)
        txt += str(tab)

        txt += "\n\n\n* ANALYSERS AXES & PARAMETERS:\n"
        analysers = self._active_analysers
        txt += analysers[0].__info__()
        for ana in analysers[1:]:
            txt += "\n" + ana.__info__().split("\n")[3]

        txt += "\n\n\n* DETECTOR AXES & PARAMETERS:\n"
        txt += self.detector.__info__() + "\n"

        return txt
