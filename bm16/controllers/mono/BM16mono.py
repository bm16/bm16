#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
BLISS controller for BM16 double crystal monochromator
=========================================================

NOTES
-----

- MonocrhomatorBase is taken from `monochromator` branch in bliss (testes with 1.7.x)
- Do not forget to update `xcalibu` -> (blissenv_dev) mamba install xcalibu=0.9.3

"""
import numpy

from bliss.controllers.monochromator import Monochromator, EnergyTrackingObject
from bliss.config import settings
from bliss.controllers.motors.icepap import Icepap


class BM16mono(Monochromator):
    """Sagittally focusing double crystal monochromator at BM16 (FAME-UHD, French CRG)"""

    def __init__(self, config):

        super().__init__(config)

        # TO BE MOVE IN BLISS MODULE
        # Load energy resolution for trajectory (KeV)
        self._traj_energy_resolution = self.config.get(
            "_par_traj_energy_resolution", 1e-3
        )

        self._sample_z0 = settings.SimpleSetting(
            f"mono_{self._name}_sample_z0", default_value=1500
        )
        self._sample_bragg0 = settings.SimpleSetting(
            f"mono_{self._name}_sample_bragg0", default_value=0
        )
        self._sample_gap0 = settings.SimpleSetting(
            f"mono_{self._name}_sample_gap0", default_value=0
        )
        self._sample_gapoffset = settings.SimpleSetting(
            f"mono_{self._name}_sample_gapoffset", default_value=0
        )

    def set_ztable(self):
        self._sample_z0.set(self._motors["sample_tz"].position)
        self._sample_bragg0.set(self._motors["bragg"].position)
        self._sample_gap0.set(self._motors["sample_gap"].position)

    @property
    def gapoffset(self):
        return self._sample_gapoffset.get()

    @gapoffset.setter
    def gapoffset(self, value):
        self._sample_gapoffset.set(value)

    @property
    def h0(self):
        return self._sample_z0.get()

    @h0.setter
    def h0(self, value):
        self._sample_z0.set(value)


class BM16monoTracker(EnergyTrackingObject):
    def _energy2tz(self, energy, config):
        tz0 = self._mono._sample_z0.get()
        bragg0 = numpy.radians(self._mono._sample_bragg0.get())
        gapoffset = self._mono.gapoffset
        gap0 = self._mono._sample_gap0.get()
        bragg_pos = numpy.radians(self._mono.energy2bragg(energy))
        tz = tz0 - 2.0 * (gap0 + gapoffset) * (numpy.cos(bragg0) - numpy.cos(bragg_pos))
        return tz

    def _tz2energy(self, tz, config):
        raise NotImplementedError

    ###################################################################
    #####
    ##### TO BE MOVED IN BLISS MODULE
    #####
    ###################################################################
    def _get_energy_from_table(self, axis_name, param_id, track):
        if axis_name not in self._motors.keys():
            raise ValueError(
                f"EnergyTrackingObject->_get_energy_from_table: {axis_name} is not a tracker"
            )
        param_ids = self._parameters[axis_name]["param_id"]
        if param_ids is None or param_id not in param_ids.keys():
            raise ValueError(
                f"EnergyTrackingObject->_get_energy_from_table: id {param_id} is not a valid id for {axis_name}"
            )

        if isinstance(track, numpy.ndarray):
            track_arr = numpy.copy(track)
        else:
            track_arr = numpy.array([track], dtype=float)
        ene_arr = numpy.copy(track_arr)

        for i in range(track_arr.size):
            step_ene = 0.1
            curr_ene = self._mono.bragg2energy(
                self._mono._motors["bragg_rotation"].position
            )
            curr_track = self._get_track_from_table(axis_name, param_id, curr_ene)
            curr_diff = abs(track_arr[i] - curr_track)
            found_it = False
            while not found_it:
                if numpy.isclose(track_arr[i], curr_track, atol=0.001):
                    ene_arr[i] = curr_ene
                    print("FOUND !!! ", track_arr[i], " ", curr_track)
                    found_it = True
                else:
                    if abs(step_ene) < 0.00001:
                        ene_arr[i] = curr_ene
                        found_it = True
                    else:
                        if abs(track_arr[i] - curr_track) > curr_diff:
                            step_ene = -(step_ene / 2)
                        curr_diff = abs(track_arr[i] - curr_track)
                        curr_ene = curr_ene + step_ene
                        curr_track = self._get_track_from_table(
                            axis_name, param_id, curr_ene
                        )

        if track_arr.size == 1:
            return ene_arr[0]

        return ene_arr

    def _get_energy_from_polynom(self, axis_name, param_id, track):
        if axis_name not in self._motors.keys():
            raise ValueError(
                f"EnergyTrackingObject->_get_track_from_polynom: {axis_name} is not a tracker"
            )
        param_ids = self._parameters[axis_name]["param_id"]
        if param_ids is None or param_id not in param_ids.keys():
            raise ValueError(
                f"EnergyTrackingObject->_get_track_from_polynom: id {param_id} is not a valid id for {axis_name}"
            )

        if isinstance(track, numpy.ndarray):
            track_arr = numpy.copy(track)
        else:
            track_arr = numpy.array([track], dtype=float)
        ene_arr = numpy.copy(track_arr)

        for i in range(track_arr.size):
            step_ene = 0.1
            curr_ene = self._mono.bragg2energy(
                self._mono._motors["bragg_rotation"].position
            )
            curr_track = self._get_track_from_polynom(axis_name, param_id, curr_ene)
            curr_diff = abs(track_arr[i] - curr_track)
            found_it = False
            while not found_it:
                if numpy.isclose(track_arr[i], curr_track, atol=0.001):
                    ene_arr[i] = curr_ene
                    found_it = True
                else:
                    if abs(step_ene) < 0.00001:
                        print("Warning: Did not find an Energy position...")
                        ene_arr[i] = curr_ene
                        found_it = True
                    else:
                        if abs(track_arr[i] - curr_track) > curr_diff:
                            step_ene = -(step_ene / 2)
                        curr_diff = abs(track_arr[i] - curr_track)
                        curr_ene = curr_ene + step_ene
                        curr_track = self._get_track_from_polynom(
                            axis_name, param_id, curr_ene
                        )

        if track_arr.size == 1:
            return ene_arr[0]

        return ene_arr
