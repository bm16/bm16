#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
BLISS controller for the Siemens S7 automaton
Same basis as Eurotherm3504

Example of YML configuration:

- name: s7mono
  class: SiemensS7
  package: bm16.controllers.siemensS7
  controller_ip: bm16s7.esrf.fr
  inputs:
  - name: ln2
    registerDec: 0
    type: "f"
    description: "LN2 level percentage"
  - name: pln2
    registerDec: 2
    type: "f"
    description: "LN2 pressure in bar"
  - name: valves
    registerDec: 6
    type: "b"
    description: "Valves status"
  - name: contacts
    registerDec: 7
    type: "b"
    description: "VR status and refill demand"

"""
from bliss.common.logtools import log_debug
from bliss.comm import modbus
from bliss.common.counter import SamplingCounter, SamplingMode
from bliss.controllers.counter import SamplingCounterController


class SiemensS7(SamplingCounterController):
    """
    SiemensS7 Counter controller
    """

    def __init__(self, name, config):
        super().__init__(name)

        self.config = config
        self.controller_ip = self.config["controller_ip"]
        self.com = modbus.ModbusTcp(self.controller_ip, port=502)

        self.inputs = {}

        self._valves = {}
        self._valves_names = ["V1", "V2", "V3", "V4", "V5", "V6", "VP"]  # , "VR"] selfa already defined in mono
        self._valves_status = {}

        for counter_conf in config.get("inputs", list()):

            counter_name = counter_conf["name"]
            counter = self.create_counter(
                SamplingCounter, counter_name, mode=SamplingMode.SINGLE
            )
            counter.com_reg = counter_conf["registerDec"]
            counter.com_dtype = counter_conf["type"]
            self.inputs[counter_name] = counter

        for valv_name in self._valves_names:
            valv_counter = self.create_counter(
                SamplingCounter, valv_name, mode=SamplingMode.SINGLE
            )
            self.inputs[valv_name] = valv_counter

    def read_all(self, *counters):
        log_debug(self, "read_all")
        plist = list()
        self.valves_status()
        for ic, c in enumerate(counters):
            if c.name not in self._valves_names:
                log_debug(
                    self,
                    f"counter: {c.name}, register: {c.com_reg} with dtype: {c.com_dtype}",
                )
                plist.append(self.com.read_holding_registers(c.com_reg, c.com_dtype))
            else:
                plist.append(self._valves_status[c.name])
        return plist

    def valves_status(self):
        self._valves_status = {}
        vc = self.counters[2]
        vc_reg, vc_dtype = vc.com_reg, vc.com_dtype
        _valves = self.com.read_holding_registers(vc_reg, vc_dtype)
        ii = 1
        for valve in range(0, 7):
            io = _valves & ii
            if io == ii:
                self._valves_status[self._valves_names[valve]] = 1
            else:
                self._valves_status[self._valves_names[valve]] = 0
            ii = ii << 1
        #add VR status
        cc = self.counters[3]
        cc_reg, cc_dtype = cc.com_reg, cc.com_dtype
        _contacts = self.com.read_holding_registers(cc_reg, cc_dtype)
        if (_contacts & 2) == 2:
            self._valves_status[self._valves_names[valve]] = 1
        else:
            self._valves_status[self._valves_names[valve]] = 0

        for valve_name in self._valves_names:
            # Conversion into "open" or "close"
            # All the valves are "normally close" (0=close, 1=open) except V2 (0=open, 1=close) .
            # VR special case: if 1 --> selfa is fully open
            #                 if 0 --> other selfa apperture
            if valve_name == "V2":
                if self._valves_status[valve_name]:
                    self._valves[valve_name] = "close"
                else:
                    self._valves[valve_name] = "open"
            elif valve_name == "VR":
                if self._valves_status[valve_name]:
                    self._valves[valve_name] = "Selfa is 100% open"
                else:
                    self._valves[valve_name] = "Selfa is close or partially open"
            else:
                if self._valves_status[valve_name]:
                    self._valves[valve_name] = "open"
                else:
                    self._valves[valve_name] = "close"
        return self._valves
