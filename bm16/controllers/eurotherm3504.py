#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
BLISS controller for the Eurotherm 3504

Example of YML configuration:

- name: euromono
  class: Eurotherm3504
  package: bm16.controllers.eurotherm3504
  controller_ip: bm16rm.esrf.fr
  inputs:
    - name: euro
      registerDec: 15360
      type: float32
      description: "temperature eurotherm regulation",
    - name: out_p
      registerDec: 22899
      type: float32
      description: "percentage output power"

"""
from bliss.common.logtools import log_debug
from bliss.comm import modbus
from bliss.common.counter import SamplingCounter, SamplingMode
from bliss.controllers.counter import SamplingCounterController


class Eurotherm3504(SamplingCounterController):
    """
    Eurotherm3504 Counter controller
    """

    def __init__(self, name, config):
        super().__init__(name)

        self.config = config
        self.controller_ip = self.config["controller_ip"]
        self.com = modbus.ModbusTcp(self.controller_ip, port=502)

        self.inputs = {}

        for counter_conf in config.get("inputs", list()):

            counter_name = counter_conf["name"]
            counter = self.create_counter(
                SamplingCounter, counter_name, mode=SamplingMode.SINGLE
            )
            counter.com_reg = counter_conf["registerDec"]
            counter.com_dtype = counter_conf["type"]
            self.inputs[counter_name] = counter

    def read_all(self, *counters):
        log_debug(self, "read_all")
        plist = list()
        for c in counters:
            log_debug(
                self,
                f"counter: {c.name}, register: {c.com_reg} with dtype: {c.com_dtype}",
            )
            plist.append(self.com.read_holding_registers(c.com_reg, c.com_dtype))

        return plist
