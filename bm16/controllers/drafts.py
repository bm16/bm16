# essais de communication avec eurotherm (_euro_wfloat de spec)
from bliss.common.logtools import log_debug


def io_write(self, reg, val):
    log_debug(self, "io_write reg=%r val=%r" % (reg, val))
    _MSG = []
    _MSG.append("0x00")
    _MSG.append("0x00")
    _MSG.append("0x00")
    _MSG.append("0x00")
    _MSG.append("0x00")
    _MSG.append("0x0B")
    _MSG.append("0xFF")
    _MSG.append("0x10")
    _MSG.append(hex((reg & 0xFF00) >> 8))
    _MSG.append(hex(reg & 0x00FF))
    _MSG.append("0x00")
    _MSG.append("0x02")  # 2 registers
    _MSG.append("0x04")
    _MSG.append(hex((self.floatrep(val) & 0xFF000000) >> 24))
    _MSG.append(hex((self.floatrep(val) & 0x00FF0000) >> 16))
    _MSG.append(hex((self.floatrep(val) & 0x0000FF00) >> 8))
    _MSG.append(hex((self.floatrep(val) & 0x000000FF)))
    print(_MSG)
    self.comm.write(_MSG.encode())
    log_debug(self, "   write :%r" % _MSG)
    return


def floatrep(self, x):
    # representation d un float en simple precision sur 32 bits (IEEE754)
    # 1 bit de signe        s
    # 8 bits d exposant     e
    # 23 bits de mantisse   m
    # x=(-1)^s*(1+m*2^(e-23))*2^(e-127)

    mk = 0x400000
    mb = 0
    s = 0

    if x < 0:
        s = 0x80000000
        x = -x
    if x != 0:
        n = numpy.log(x) / numpy.log(2)
        e = int(n)
        if n < 0 and (n - int(n)) != 0:
            e = e - 1
        m = pow(2, n - e) - 1

        while mk != 0:
            m = m * 2
            if m >= 1:
                mb = mb | mk
                m -= 1
            mk = mk >> 1
        # gestion arrondi
        if m >= 0.5:
            mb += 1
        e = (e + 127) << 23
        v = s | e | mb
    else:
        v = 0x00000000

    return v
