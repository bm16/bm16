import struct
import functools
import weakref
import numpy
from collections import namedtuple
from enum import Enum
import time
import gevent.lock
import gevent
from bliss.comm.util import get_comm, TCP


from bliss.common import session

from bliss.common.logtools import log_info, log_warning, log_error, log_debug, log_debug_data
from bliss.common.counter import SamplingCounter
from bliss.controllers.counter import SamplingCounterController
from bliss.scanning.acquisition.counter import SamplingCounterAcquisitionSlave
from bliss.config.channels import Channel

from bliss.shell.standard import debugon

"""
Class for CAEN charge integration Picoammeter.
https://www.caenels.com/wp-content/uploads/2015/04/AH401D_UsersManual_V1.5.pdf
Works for B or D type

yml file :

    plugin: bliss
    name: pico_d2
    class: Ah401
    package: bm32.controllers.ah401
    tcp:
        url: bm32pico2
    unit: nA
    counters:
        - counter_name: ch11
        channel: 1
        - counter_name: ch12
        channel: 2
        - counter_name: ch13
        channel: 3
        - counter_name: ch14
        channel: 4
"""

AHRANGE = {
    "0": (1.8, 2, 1e-9, "nA", "nC"),
    "1": (50, 50, 1e-12, "pA", "pC"),
    "2": (100, 100, 1e-12, "pA", "pC"),
    "3": (150, 150, 1e-12, "pA", "pC"),
    "4": (200, 200, 1e-12, "pA", "pC"),
    "5": (250, 250, 1e-12, "pA", "pC"),
    "6": (300, 300, 1e-12, "pA", "pC"),
    "7": (350, 350, 1e-12, "pA", "pC"),
}
AHUNIT = {"µA": 1e-6, "nA": 1e-9, "pA": 1e-12}
AH401SAT = 1048500
AHRANGEDEC = {"B": 0, "D": 1, "MUL": 2, "A": 3, "C": 4}
DEFAULT_PORT = 10001


class Ah401AcquisitionSlave(SamplingCounterAcquisitionSlave):
    def prepare_device(self):
        self.device.prepare(self.count_time)

    def start_device(self):
        # start is called at each scan point IF start_once=True (by default if npoints > 0),
        # or only at first if start_once=False (default if npoints=0)
        #
        # self.device.start()
        pass

    def stop_device(self):
        self.device.stop()

    def trigger(self):
        # trigger is always called at each scan point
        self.device.stop()
        self.device.start()

        self._trig_time = time.time()
        self._event.set()
        # print("trig")


class Ah401(SamplingCounterController):
    def __init__(self, name, config):
        super().__init__(name)
        self.config = config
        self.comm = get_comm(config, ctype=TCP, port=DEFAULT_PORT)
        self.comm._eol = "\n"
        self.comm._timeout = 0.2
        self.counters_list = list()
        self.buffer = list()
        self.settings = namedtuple(
            "settings",
            [
                "version",
                "type",
                "range",
                "Imax",
                "unit",
                "integration",
                "half",
                "trigger",
            ],
        )
        self.convfact = {}
        self.settings.unit = config.get("unit")
        self.init()

        for counter_conf in config.get("counters", list()):
            unit = counter_conf.get_inherited("unit")
            cname = counter_conf["counter_name"]
            counter = self.create_counter(SamplingCounter, cname, unit=unit)
            counter.channel = counter_conf["channel"]

    def get_current_parameters(self):
        return None

    def get_acquisition_object(self, acq_params, ctrl_params, parent_acq_params):

        return Ah401AcquisitionSlave(self, ctrl_params=ctrl_params, **acq_params)

    def init(self):
        self._ioRW("ACQ OFF")
        self.settings.version = self._ioRW("VER ?")
        # read the range and initialize settings_list
        # type B : range is common to the four acquisition_channels
        # type D : range for channels 1 & 2 and 3 & 4
        self.settings.range = self._ioRW("RNG ?")

        self.settings.half = self._ioRW("HLF ?")
        self.settings.integration = int(self._ioRW("ITM ?")) / 10
        self.settings.trigger = self._ioRW("TRG ?")
        self._ioRW("BIN ON")

        # use the answer to the RNG command to see which device is connected
        if len(self.settings.range) == 1:
            self.settings.type = "B"
            for i in range(4):
                self.convfact[i] = (
                    AHRANGE[self.settings.range][AHRANGEDEC[self.settings.type]]
                    * AHRANGE[self.settings.range][2]
                    / (2 ** 20)
                    / self.settings.integration
                    / AHUNIT[self.settings.unit]
                    * 1000
                )

        if len(self.settings.range) == 2:
            self.settings.type = "D"
            i = 0
            for r in self.settings.range:
                self.convfact[i] = (
                    AHRANGE[r][AHRANGEDEC[self.settings.type]]
                    * AHRANGE[r][2]
                    / (2 ** 20)
                    / self.settings.integration
                    / AHUNIT[self.settings.unit]
                    * 1000
                )
                self.convfact[i + 1] = self.convfact[i]
                i += 2

        self.settings.Imax = list()
        for r in self.settings.range:
            self.settings.Imax.append(
                (
                    "{0:.3f} nA".format(
                        AHRANGE[r][AHRANGEDEC[self.settings.type]]
                        / self.settings.integration
                        * 1000
                    )
                )
            )

    def __info__(self):
        self.init()
        info = list()
        for i in self.settings._fields:
            if i == "range":
                for r, g in zip(self.settings.range, ("1&2", "3&4")):
                    if self.settings.type == "B":
                        g = ""
                    info.append(
                        "{0:5s} {1:6s}: {2} {3}".format(
                            i,
                            g,
                            AHRANGE[r][AHRANGEDEC[self.settings.type]],
                            AHRANGE[r][AHRANGEDEC["C"]],
                        )
                    )
            else:
                if i == "integration":
                    u = "ms"
                else:
                    u = ""
                info.append("{0:12s}: {1} {2}".format(i, getattr(self.settings, i), u))
        return "\n".join(info)

    '''def setrange(self, *range_0_7, chan=None):
        """Testrring version for D type"""
        if len(range_0_7) == 0:
            print("Available ranges : ")
            for i in AHRANGE:
                print(
                    " %s: %s %s "
                    % (
                        i,
                        AHRANGE[i][AHRANGEDEC[self.settings.type]],
                        AHRANGE[i][AHRANGEDEC["C"]],
                    )
                )
            if self.settings.type == "D":
                cur_rng=str(self._ioRW("RNG ?"))
                print("Current range configuration is : "+cur_rng)
                print("        --> ch12 ("+cur_rng[0]+"): "+str(AHRANGE[cur_rng[0]][AHRANGEDEC[self.settings.type]])+str(AHRANGE[cur_rng[0]][AHRANGEDEC["C"]]))
                print("        --> ch34 ("+cur_rng[1]+"): "+str(AHRANGE[cur_rng[1]][AHRANGEDEC[self.settings.type]])+str(AHRANGE[cur_rng[1]][AHRANGEDEC["C"]]))

        elif chan==None:
            rng = str(*range_0_7)
            if self.settings.type == "D" and len(rng) == 1:
                rng = rng + rng
                log_info(self, "Use same range for the 4 channels")
            self._ioRW("RNG " + rng)
            self.init()

        else:
            rng = str(*range_0_7)
            cur_rng=str(self._ioRW("RNG ?"))
            if chan=="ch12":
                self._ioRW("RNG " + rng + cur_rng[1])
            elif chan=="ch34":
                self._ioRW("RNG " + cur_rng[0] + rng)
            else:
                print("Channel aliases are ch12 and ch34")'''

    def setrange(self, *range_0_7):
        """bla bla"""
        if len(range_0_7) == 0:
            print("Available ranges : ")
            for i in AHRANGE:
                print(
                    " %s: %s %s "
                    % (
                        i,
                        AHRANGE[i][AHRANGEDEC[self.settings.type]],
                        AHRANGE[i][AHRANGEDEC["C"]],
                    )
                )

            if self.settings.type == "D":
                cur_rng = str(self._ioRW("RNG ?"))
                print('Current range configuration is : "' + cur_rng + '"')
                print(
                    "        --> ch12 ("
                    + cur_rng[0]
                    + "): "
                    + str(AHRANGE[cur_rng[0]][AHRANGEDEC[self.settings.type]])
                    + " "
                    + str(AHRANGE[cur_rng[0]][AHRANGEDEC["C"]])
                )
                print(
                    "        --> ch34 ("
                    + cur_rng[1]
                    + "): "
                    + str(AHRANGE[cur_rng[1]][AHRANGEDEC[self.settings.type]])
                    + " "
                    + str(AHRANGE[cur_rng[1]][AHRANGEDEC["C"]])
                )

        rng = str(*range_0_7)

        if self.settings.type == "D" and len(rng) == 1:
            rng = rng + rng
            log_info(self, "Use same range for the 4 channels")
        self._ioRW("RNG " + rng)
        self.init()

    def setintegration(self, integration_time_in_ms):
        self._ioRW("ITM " + str(integration_time_in_ms * 10))
        self.init()

    def sethalfmodeon(self):
        self._ioRW("HLF ON")
        self.init()

    def sethalfmodeoff(self):
        self._ioRW("HLF OFF")
        self.init()

    def setunit(self, unit):
        if unit in AHUNIT:
            self.settings.unit = unit
        else:
            log_warning(self, "Can't set unit %s" % unit)
        self.init()

    def read_all(self, *counters):
        log_debug(
            self,
            "Ah401 read_all - point {} of {} + {}".format(
                self.nread, self.ntoread, self.nread_mean
            ),
        )
        if self.nread < self.ntoread:
            buf = self.comm.read(12)
            self.nread += 1

            log_debug(self, "read : %s" % buf)
            values = list()
            rvalues = list()
            for i in range(4):
                rawread = buf[0 + 3 * i] + buf[1 + 3 * i] * 256 + buf[2 + 3 * i] * 65536
                log_debug_data(self, "channel %d : " % i, " ==> %d" % rawread)
                if rawread > AH401SAT and not self.saturation:
                    log_warning(self, "Saturation on Ah401 channel %d" % i)
                    log_warning(self, "      read : %s - raw %i:" % (buf, rawread))
                    self.saturation = True
                    pass
                values.append((rawread - 4096) * self.convfact[i])
            for c in counters:
                rvalues.append(values[c.channel - 1])
            if self.nread == 1:
                self.sum = rvalues
            else:
                for i in range(len(rvalues)):
                    self.sum[i] += rvalues[i]

            # export counter values in channels
            for i, val in enumerate(rvalues):
                Channel(f"{self.name}_{self.counters[i].name}", val)

            return rvalues
        else:
            self.nread_mean += 1
            return [n / self.nread for n in self.sum]

    def prepare(self, count_time):

        # self.ntoread = 100 * int(count_time * 1000.0 / self.settings.integration)
        self.ntoread = int(count_time * 1000.0 / self.settings.integration)
        if self.settings.half == "ON":
            self.ntoread = int(self.ntoread / 2)
        log_debug(
            self,
            "Ah401 prepare_acquisition for {} integration periods".format(self.ntoread),
        )
        if self.ntoread == 0:
            log_warning(
                self,
                "Counting time is smaller than the AH401 integration time; using integration time : {0} ms".format(
                    self.settings.integration
                ),
            )
            self.ntoread = 1
        # self._ioRW("NAQ " + str(self.ntoread + 1))
        self._ioRW("NAQ " + str(self.ntoread))
        # self.nread = 0
        self.saturation = False

        return self.ntoread

    def start(self):
        log_debug(self, "Ah401 start")
        self.nread = 0
        self.nread_mean = 0
        self._ioRW("ACQ ON")

    def stop(self):
        log_debug(self, "Ah401 stop")
        self._ioRW("ACQ OFF")

    def _ioRW(self, cmd):
        self.comm.flush()
        cmd = cmd + "\r"
        try:
            ans = self.comm.write_readline(cmd.encode()).decode()
        except Exception:
            ans = ""

        log_debug_data(self, "Ah401._ioRW : ", " send:%s read:%s" % (cmd, ans))
        if "NAK" not in ans:
            if "?" in cmd:
                if "VER" in cmd:
                    try:
                        return ans.split(" ")[1]
                    except IndexError:
                        log_error(self, "not responding -> PHYSICALLY RESTART THE DEVICE!")
                else:
                    return ans.split(" ")[1][:-1]
