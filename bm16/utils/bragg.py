#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Utility functions around Bragg's law 
"""

import math
import numpy as np

# GLOBAL VARIABLES #
HC = 1.2398418743309972e-06  # eV * m
ALAT_SI = 5.431065  # Ang at 25C
ALAT_GE = 5.6579060  # Ang at 25C

# UTILITY FUNCTIONS #
def d_cubic(a, hkl):
    """d-spacing for a cubic lattice"""
    h, k, l = hkl[0], hkl[1], hkl[2]
    d2m = (h ** 2 + k ** 2 + l ** 2) / a ** 2
    if d2m == 0:
        return 0
    else:
        return math.sqrt(1 / d2m)


def get_dspacing(mat, hkl):
    """Get d-spacing for Si or Ge and given reflection (hkl)"""
    if mat == "Si":
        dspacing = d_cubic(ALAT_SI, hkl)
    elif mat == "Ge":
        dspacing = d_cubic(ALAT_GE, hkl)
    else:
        print("ERROR get_dspacing: available materials -> 'Si' 'Ge'")
        dspacing = 0
    return dspacing


def kev2wlen(energy):
    """Convert photon energy (E, keV) to wavelength ($\lambda$, \AA$^{-1}$)"""
    try:
        return (HC / energy) * 1e7
    except:
        return 0


def wlen2kev(wlen):
    """Convert photon wavelength ($\lambda$, \AA$^{-1}$) to energy (E, keV)"""
    try:
        return (HC / wlen) * 1e7
    except:
        return 0


def kev2ang(ene, d=0, deg=True):
    """Convert energy (keV) to Bragg angle (deg/rad) for given d-spacing (\AA)"""
    if d == 0:
        print("ERROR kev2deg: d-spacing is 0")
        return 0
    else:
        _ang = math.asin((kev2wlen(ene)) / (2 * d))
        if deg is True:
            _ang = math.degrees(_ang)
        return _ang


def ang2kev(theta, d=0, deg=True):
    """Bragg angle (deg/rad) to energy (keV) for given d-spacing (\AA)"""
    if deg is True:
        theta = math.radians(theta)
    return wlen2kev(2 * d * math.sin(theta))

