# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
YAML_ configuration example:

.. code-block:: yaml
    - class: TrigCounterController
      package: pel.TrigCounter
      name: interf
      high_time: 0.3
      period: 0.6
      nb_pulses: 10
      trigger_device:
          type: opiom
          name: $OPIOM_NAME
          counter_number: 1     # CNT 1
"""
import gevent

from bliss.common.counter import IntegratingCounter
from bliss.controllers.counter import IntegratingCounterController
from bliss.scanning.acquisition.counter import IntegratingCounterAcquisitionSlave

from bliss.config.settings import HashSetting


class TrigCounterController(IntegratingCounterController):
    """Trigger with OPIOM card for measurements with PEL"""

    def __init__(self, name, config):
        super().__init__(name)
        self.config = config
        trig_config = config["trigger_device"]

        self._opiom_card = trig_config.get("name")
        self._opiom_counter = trig_config.get("counter_number", 1)  # default 1
        high_time = config.get("high_time", 1)
        period = config.get("period", 1)
        nb_pulses = config.get("nb_pulses", 1)

        self.__trigger_clock = config.get("_clock", 2000000)

        self._counter_config = HashSetting(
            "%s:counter_config" % self.name,
            default_values={
                "high_time": high_time,
                "period": period,
                "nb_pulses": nb_pulses,
            },
        )

        counter_name = f"out{self._opiom_counter}"
        self.create_counter(IntegratingCounter, counter_name)

    def __info__(self):
        params = " ".join(["%s=%s" % (k, v) for k, v in self._counter_config.items()])
        return "%s %s: %s" % (self.__class__.__name__, self.name, params)

    @property
    def clock(self):
        return self.__trigger_clock

    @property
    def high_time(self):
        """
        High time in second for the pulse
        """
        return self._counter_config["high_time"]

    @high_time.setter
    def high_time(self, value):
        self._counter_config["high_time"] = value

    @property
    def period(self):
        """
        period in second for the pulse (high + low level)
        """
        return self._counter_config["period"]

    @period.setter
    def period(self, value):
        self._counter_config["period"] = value

    @property
    def nb_pulses(self):
        """
        Nb pulses generated when trigged
        """
        return self._counter_config["nb_pulses"]

    @nb_pulses.setter
    def nb_pulses(self, value):
        self._counter_config["nb_pulses"] = value

    def get_values(self, from_index, *counters):
        return [[self.nb_pulses]]

    def get_acquisition_object(self, acq_params, ctrl_params, parent_acq_params):
        return TrigCounterAcquisitionSlave(self, ctrl_params=ctrl_params, **acq_params)


class TrigCounterAcquisitionSlave(IntegratingCounterAcquisitionSlave):
    def prepare_device(self):
        opiom = self.device._opiom_card
        counter = self.device._opiom_counter
        high_time = self.device.high_time * self.device.clock
        low_time = (self.device.period - self.device.high_time) * self.device.clock
        nb_pulses = self.device.nb_pulses
        opiom.comm("CNT %d RESET" % counter)
        opiom.comm(
            "CNT %d CLK2 PULSE %d %d %d" % (counter, high_time, low_time, nb_pulses)
        )
        # gevent.sleep(nb_pulses * self.device.period)

    # def add_counter(self, counter):
    #     self.counter = counter
    #     self.channels.append(AcquisitionChannel(self.counter.name, numpy.float, ()))

    def start_device(self):
        if not self.parent:
            self.trigger()

    def stop_device(self):
        opiom = self.device._opiom_card
        counter = self.device._opiom_counter
        opiom.comm("#CNT %d STOP" % counter)

    def trigger(self):
        # self.trigger_slaves()
        opiom = self.device._opiom_card
        counter = self.device._opiom_counter
        opiom.comm("CNT %d START" % counter)
        # values = {self.device.counters[0].name: self.device.get_values(0)}
        # self.channels.update(values)

    def wait_ready(self):
        opiom = self.device._opiom_card
        counter = self.device._opiom_counter
        comm = "?SCNT %d" % counter
        while opiom.comm(comm) == "RUN":
            gevent.idle()
