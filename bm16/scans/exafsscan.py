import tabulate
import numpy
import json

from bliss.common.scans import pointscan
from bliss.scanning.group import Sequence
from bliss.config.conductor import client

from bliss.config.static import Config


class ExafsScan:
    def __init__(self, name, config):
        self.__name = name
        self._config = config
        self._scan_path = "myscans"
        self.regions = config.get('regions', list)
        self.element = config.get('element')
        self.edge = config.get('edge')
        self.edge_energy = config.get('edge_energy', float)
        self.motor = config.get('motor')
        
        self._compute_scan_positions()
        
    def __info__(self):
        info = f"{len(self.regions)}-REGIONS SCAN"
        info += "\n"

        attr = [["Motor:", self.motor.name]]
        
        for e in ["element", "edge", "edge_energy"]:
            val = getattr(self, e, None)
            if val:
                attr.append([f"{e.title()}:", val])

        attrtable = tabulate.tabulate(
            attr, tablefmt="plain", stralign="left"
        )

        title = ["Region", "start", "end", "step", "counting_time", "nb_steps", "total_counting_time"]
        
        regions = []
        total_steps = 0
        total_counting_time = 0
        
        for i, region in enumerate(self.regions, 1):
            regions.append([f"#{i}", region["start"], region["end"], region["step"], region["counting_time"], region["nb_steps"], region["counting_time"] * region["nb_steps"]])
            total_steps += region["nb_steps"]
            total_counting_time += region["nb_steps"] * region["counting_time"]
        
        regions.append(['------', None, None, None, None, "------", "------"])
        regions.append(['total', None, None, None, None, total_steps, total_counting_time])

        regiontable = tabulate.tabulate(
            regions, headers=title, tablefmt="plain", stralign="right"
        )

        return info + "\n" + attrtable + "\n\n" + regiontable
    
    def _compute_scan_positions(self):
        end_of_last_region = None
        for region in self.regions:
            region["start"] = region.get("start", end_of_last_region)
            end_of_last_region = region["end"]
            
            if region["unit"] == "energy":
                positions = []
                pos = region["start"]

                while pos <= region["end"]:
                    positions.append(pos)
                    pos += region["step"]

                region.update(positions=positions)
                region.update(nb_steps=len(positions))
                
            if region["unit"] == "wavenumber":
                raise NotImplementedError

    def scan(self, *counters, **kwargs):
        self._compute_scan_positions()

        kwargs.update(run=False)
        seq = Sequence()
        with seq.sequence_context() as scan_seq:
            for region in self.regions:
                s = pointscan(self.motor, numpy.array(region["positions"]), region["counting_time"], *counters, **kwargs)
                scan_seq.add_and_run(s)
                
        return seq

    def export(self, filename):
        obj = dict()
        obj["element"] = self.element
        obj["edge"] = self.edge
        obj["edge_energy"] = self.edge_energy
        obj["motor"] = self.motor.name
        obj["regions"] = []
        for region in self.regions:
            obj["regions"].append(region.to_dict())
        for region in obj["regions"]:
            del region["positions"]
        
        content = json.dumps(obj, indent=4)
        
        client.set_config_db_file(filename, content)

    def load(self, filename):
        content = client.get_config_file(filename).decode("utf-8")
        obj = json.loads(content)
        
        cfg = Config()
        
        self.element = obj["element"]
        self.edge = obj["edge"]
        self.edge_energy = obj["edge_energy"]
        self.motor = cfg.get(obj["motor"]) 
        self.regions = obj["regions"]
        self._compute_scan_positions()
