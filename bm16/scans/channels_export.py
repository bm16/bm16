#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Scan preset to export channels among sessions
=============================================
"""
from bliss.scanning.scan import ScanPreset
from bliss.config.channels import Channel


class CounterChannelScanPreset(ScanPreset):
    def __init__(self, *counters):
        super().__init__()

        self.counters = counters

    def prepare(self, scan):
        self.connect_data_channels(self.counters, self.emit_counter_channel)

    def emit_counter_channel(self, counter, channel_name, data):
        Channel(channel_name, value=data[-1])
