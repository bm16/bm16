#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Scans for metrology measurements done by PEL at BM16
====================================================

.. note:: This file is taken from :mod:`pel.PELscans`

"""

from bliss.common.scans import ascan
# from bliss.common.plot import *
from bliss.shell.standard import umv, ct, umvr, sync
import gevent

def PELscan(mot, p_st, p_en, nbp, t_int, *counters, **kwargs):

    display_help = kwargs.pop("help", False)

    if display_help:
        print(
            "PELscan(mot, start, stop, nbp, int_time, interf1, counter.... nbscan=n, backlash=val, bidir=True/False, sleep_time=t(s)"
        )
        return

    nbs = kwargs.pop("nbs", 1)
    backlash = kwargs.pop("backlash", 0)
    bidir = kwargs.pop("bidir", False)

    return_scan = kwargs.get("return_scan", False)
    kwargs["return_scan"] = True

    for loop in range(nbs):
        if bidir and loop % 2 == 1:
            pos_bckl = p_en + backlash
            pos_start = p_en
            pos_end = p_st
        else:
            pos_bckl = p_st - backlash
            pos_start = p_st
            pos_end = p_en

        print('Move motor "%s" to backlash position %g' % (mot.name, pos_bckl))
        umv(mot, pos_bckl)
        # mot.move(pos_bckl)

        print('Move motor "%s" to 1st scan position %g' % (mot.name, pos_start))
        umv(mot, pos_start)
        # mot.move(pos_start)

        print("Waiting time for 1st point %d (s)" % (t_int,))
        gevent.sleep(t_int)

        print("Scan #%d (/%d) from %g to %g\n\n" % (loop + 1, nbs, pos_start, pos_end))
        scan = ascan(mot, pos_start, pos_end, nbp, t_int, *counters, **kwargs)

        # data = scan.get_data()

        # plot motor pos vs. counters by default
        # do not plot interferometer since it doesn't give a useful result
        # counters_to_plot = [
        #    name
        #    for name in data.dtype.fields
        #    if name not in ("timestamp", mot.name) and not name.startswith("interf")
        # ]

        # Plotting de-activated by CG 6 aug. 2018... causes crash with flint :(
        #        x = data[mot.name]
        #        y = data[counters_to_plot]
        #        plot(y, x=x)


def PELmim(mot, p_start, p_end, n_pts, t_wait, counter, **kwargs):
    """MIM scan"""
    p_incrm = (p_end - p_start)/n_pts
    print(f"MIM increment is: {p_incrm}")
    mot_pos = p_start
    i_pt = 0
    while i_pt < n_pts+1:
        umv(mot, mot_pos)
        print("Waiting %d (s)..." % (t_wait,))
        gevent.sleep(t_wait)
        print(f"Pulsing {counter.nb_pulses} (times)...")
        ct(0.1, counter)
        mot_pos += p_incrm
        i_pt += 1


def PELrepeat(mot, n_times, p_incrm, counter, nb_pulses=1, t_wait=1, **kwargs):
    """Repeat loop"""
    print(f"Repeat times: {n_times}")
    i_pt = 0
    counter.nb_pulses = nb_pulses
    while i_pt < n_times:
        umvr(mot, p_incrm)
        gevent.sleep(t_wait) 
        sync()     
        umvr(mot, -1*p_incrm)
        gevent.sleep(t_wait)
        sync()     
        print(f"Pulsing {counter.nb_pulses} (times)...")
        ct(0.1, counter)
        i_pt += 1

