import tabulate
import numpy

from bliss.common.scans import pointscan
from bliss.scanning.group import Sequence


class ExafsScan:
    def __init__(self, name, config):
        self.__name = name
        self._config = config
        self.regions = config.get('regions', list)
        self.element = config.get('element')
        self.edge = config.get('edge')
        self.edge_energy = config.get('edge_energy', float)
        self.motor = config.get('motor')
        
    def __info__(self):
        info = f"{len(self.regions)}-REGIONS SCAN"
        info += "\n"

        attr = [["Motor:", self.motor.name]]
        
        for e in ["element", "edge", "edge_energy"]:
            val = getattr(self, e, None)
            if val:
                attr.append([f"{e.title()}:", val])

        attrtable = tabulate.tabulate(
            attr, tablefmt="plain", stralign="left"
        )

        title = ["Region", "start", "end", "step", "counting_time"]
        
        regions = []
        for i, region in enumerate(self.regions, 1):
            regions.append([f"#{i}", region["start"], region["end"], region["step"], region["counting_time"]])

        regiontable = tabulate.tabulate(
            regions, headers=title, tablefmt="plain", stralign="right"
        )

        return info + "\n" + attrtable + "\n\n" + regiontable
    
    def _compute_scan_positions(self):
        for region in self.regions:
            
            if region["unit"] == "energy":
                positions = []
                pos = region["start"]

                while pos <= region["end"]:
                    positions.append(pos)
                    pos += region["step"]

                region.update(positions=positions)
                
            if region["unit"] == "wavenumber":
                raise NotImplementedError

    def scan(self, *counters, **kwargs):        
        self._compute_scan_positions()

        kwargs.update(run=False)
        seq = Sequence()
        with seq.sequence_context() as scan_seq:
            for region in self.regions:
                s = pointscan(self.motor, numpy.array(region["positions"]), region["counting_time"], *counters, **kwargs)
                scan_seq.add_and_run(s)
                
        return seq
