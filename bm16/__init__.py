#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""bm16: BLISS code for bm16 (FAME-UHD beamline, French CRG)

Naming
======
* classes MixedUpperCase
* varables lowerUpper _or_ lower
* functions underscore_separated _or_ lowerUpper

"""
from __future__ import absolute_import, print_function, division

__version__ = "2021.1"

import os

_libDir = os.path.dirname(os.path.realpath(__file__))

__pkgs__ = [
    "bm16",
    "bm16.macros",  # beamline specific macros
    "bm16.controllers",  # beamline specific controllers
    "bm16.scans", # custom scans
    "bm16.utils", # custom utilities
]

if __name__ == "__main__":
    pass
