
import numpy
import sys
import gevent
import time

from bliss.scanning.scan import ScanState, DataWatchCallback, WatchdogCallback
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.toolbox import ChainBuilder
from bliss.scanning.acquisition.musst import MusstAcquisitionSlave
from bliss.scanning.acquisition.motor import MotorMaster
from bliss.data.node import DataNodeContainer
from bliss.controllers.counter import CalcCounterController, SamplingCounterController
from bliss.controllers.ct2.device import AcqMode
from bliss.controllers.ct2.client import CT2Controller
from bliss.controllers.musst import musst
from bliss.controllers.emh import EMH
from bliss.controllers.mca.base import TriggerMode, BaseMCA
from bliss.controllers.lima.lima_base import Lima
from bliss.controllers.pepu import PEPU, Signal
from bliss.controllers.monochromator import Monochromator
from bliss.controllers.ah401 import Ah401CC
from bliss.controllers.musst import (
    MusstIntegratingCounterController,
    MusstSamplingCounterController,
)
from bliss.controllers.motor import CalcController

#from bliss.controllers.speedgoat.speedgoat_client import SpeedgoatCountersController
from bliss.controllers.speedgoat.speedgoat_bliss import SpeedgoatCountersController

from bm16.mono_scans.scan_tools_musst import musst_get_master


#######################################################################
#####
##### MUSST counters for continuous scans
#####
class ContScanMusstCounters:

    def __init__(self, name, config):

        # internal parameter
        self._name = name
        self._config = config

        # MUSST card to get counters name
        self._musst = config.get("musst", None)
        if self._musst is None:
            raise RuntimeError("ContScan: No musst device defined")

        # Get Musst Counters for Continuous scans
        musst_config = self._config.get("musst_counters", None)
        if musst_config is not None:
            self._musst_counter = {}
            for musst_cnt in musst_config:
                counter = musst_cnt.get("counter", None)
                if counter is not None:
                    self._musst_counter[counter.name] = self._musst.counters[counter.name]

        # Get Musst Calculated Counters for Continuous scans
        musst_calc_config = self._config.get("musst_calc_counters", None)
        if musst_calc_config is not None:
            self._musst_calc_counter = {}
            for musst_calc in musst_calc_config:
                counter = musst_calc.get("counter", None)
                if counter is not None:
                    self._musst_calc_counter[counter.name] = counter
        else:
            self._musst_calc_counter = None

#######################################################################
#####
##### Store Scan Parameters and create defaults
#####
def scantool_set_scan_parameters(
    motor=None,
    start=numpy.nan,
    stop=numpy.nan,
    positions=None,
    points=1,
    time=None,
    trig_start="TIME",
    trig_point="TIME",
    trajectory_mode = None,
    undulator_master = None,
):

    scan_par = {
        "motor": motor,
        "positions": positions,
        "start": start,
        "stop": stop,
        "points": points,
        "time": time,
        "expo_time": time,
        "trig_start": trig_start,
        "trig_point": trig_point,
        "trajectory_mode": trajectory_mode,
        "undulator_master": undulator_master,
    }

    return scan_par


#######################################################################
#####
##### Build Continuous scan Acquisition Master
#####
def scantool_contscan_get_acquisition_master(motor, scan_par):

    if motor is None:
        return None

    if isinstance(motor, Monochromator):
        master = motor._get_monochromator_acquisition_master_motor(
            scan_par["start"],
            scan_par["stop"],
            scan_par["points"],
            scan_par["time"],
            scan_par["trajectory_mode"],
            scan_par["undulator_master"],
        )
        return master

    if motor == scan_par["motor"]:
        master = MotorMaster(
            motor,
            scan_par["start"],
            scan_par["stop"],
            scan_par["points"] * scan_par["time"],
        )
        return master

    master = motor.controller.get_acquisition_master(
            scan_par["start"],
            scan_par["stop"],
            scan_par["points"],
            scan_par["time"],
        )
    return master

#######################################################################
#####
##### Build Continuous scan Acquisition Chain
#####
def scantool_contscan_get_acquisition_chain(
    master,
    musst,
    scan_par,
    musst_calc_channels,
    counters
):
    """
    Create chain for acquisition devices

    Input Params.
        - master
        - musst: Bliss object which synchronize the scan
        - motor: Bliss motor to be scanned
        - scan_par = {}
            "start"        : start position of the scan
            "stop"         : stop position of the scan
            "points"       : Number of points in the scan
            "time"         : Integration time(s) pwer points
            "trig_start"   : type of synchronization ("TIME"/"POSITION")
                             for start point
            "trig_point"   : type of synchronization ("TIME"/"POSITION")
                             for all other points
        - musst_calc_chan  : MUSST calc counters to be added
        - counter          : Counter asked by users
    """

    chain = AcquisitionChain(parallel_prepare=True)
    builder = ChainBuilder(counters)

    ###################################
    # MUSST
    #
    channel_list = []
    name_list = {}
    for node in builder.get_nodes_by_controller_type(
        MusstIntegratingCounterController
    ):
        for cnt in node.counters:
            ch = cnt.channel
            if ch.lower() == "timer":
                ch = 0
            else:
                ch = int(ch.split("CH")[1])
            channel_list.append(ch)
            name_list[ch] = cnt.name
    for node in builder.get_nodes_by_controller_type(
        MusstSamplingCounterController
    ):
        for cnt in node.counters:
            ch = cnt.channel
            if ch.lower() == "timer":
                ch = 0
            else:
                ch = int(ch.split("CH")[1])
            channel_list.append(ch)
            name_list[ch] = cnt.name

    musst_master = musst_get_master(musst, scan_par, channel_list)

    if len(channel_list) > 0:
        store_list = []
        channel_list.sort()
        for i in range(len(channel_list)):
            store_list.append(name_list[channel_list[i]])
        musst_acq = MusstAcquisitionSlave(musst, store_list=store_list)
        chain.add(musst_master, musst_acq)

        # Add Musst Channels Calc Counters
        if musst_calc_channels is not None:
            for name, calc in musst_calc_channels.items():
                if calc.musst_ch_name in store_list:
                    acq_slave = calc.get_acquisition_slave(musst_acq, scan_par)
                    chain.add(musst_master, acq_slave)

    # Add musst in the acquisition chain
    if master is not None:
        chain.add(master, musst_master)

    ###################################
    # FalconX
    #
    acq_params = {
        "npoints": scan_par["points"],
        "trigger_mode": TriggerMode.SYNC,
        "read_all_triggers": True,
        "block_size": 10,
    }

    for node in builder.get_nodes_by_controller_type(BaseMCA):
        node.set_parameters(acq_params=acq_params)
        chain.add(musst_master, node)

    ###################################
    # Lima
    #
    acq_params = {
        "acq_expo_time": scan_par["expo_time"],
        "acq_nb_frames": scan_par["points"] + 1,
        "acq_trigger_mode": "EXTERNAL_TRIGGER_MULTI",
    }
    ctrl_params = {
        "saving_frame_per_file": scan_par["points"] + 1,
        "saving_format": "EDF"
    }

    for node in builder.get_nodes_by_controller_type(Lima):
        node.set_parameters(acq_params=acq_params, ctrl_params=ctrl_params)
        chain.add(musst_master, node)

    ###################################
    # EMH
    #
    acq_params = {
        "count_time": scan_par["time"],
        "npoints": scan_par["points"] + 1,
        "trigger_type": "HARDWARE",
        "trigger": "DIO_1",
    }
    for node in builder.get_nodes_by_controller_type(EMH):
        node.set_parameters(acq_params=acq_params)
        chain.add(musst_master, node)

    ###################################
    # P201
    #
    acq_node_params = {
        "npoints": scan_par["points"] + 1,
        "acq_mode": AcqMode.ExtTrigReadout,
        "acq_expo_time": scan_par["time"],
        "read_all_triggers": True,
    }
    acq_child_params = {
        "count_time": scan_par["time"],
        "npoints": scan_par["points"] + 1,
    }

    p201_node = musst_master
    for node in builder.get_nodes_by_controller_type(CT2Controller):
        node.set_parameters(acq_params=acq_node_params)
        p201_node = node
        for child_node in node.children:
            child_node.set_parameters(acq_params=acq_child_params)
        chain.add(musst_master, node)

    ###################################
    # PEPU
    #
    acq_node_params = {
        "npoints": scan_par["points"] + 1,
        "start": Signal.SOFT,
        "trigger": Signal.DI2,
    }
    for node in builder.get_nodes_by_controller_type(PEPU):
        node.set_parameters(acq_params=acq_node_params)
        chain.add(musst_master, node)

    ###########################################
    # from bliss.controllers.ah401 import Ah401
    #
    acq_params = {
        "trigger_mode": "HARDWARE",
        "npoints": scan_par["points"] + 1,
    }
    for node in builder.nodes:
        if isinstance(node.controller, Ah401CC):
            cntr = node.controller.ah401
            if cntr.half_mode:
                cntr.integration_time = scan_par["time"]/2.0 - 0.001
                acq_params["count_time"] = scan_par["time"]/2.0 - 0.001
            else:
                cntr.integration_time = scan_par["time"] - 0.001
                acq_params["count_time"] = scan_par["time"] - 0.001
            node.set_parameters(acq_params=acq_params)
            chain.add(musst_master, node)

    ###################################
    # Speedgoat, CalcCounterController
    #
    acq_params = {
        "count_time": scan_par["time"],
        "npoints": scan_par["points"] + 1,
        "trigger_type": "HARDWARE",
    }
    for node in builder.nodes:
        if isinstance(
            node.controller, (SpeedgoatCountersController, CalcCounterController)
        ):
            node.set_parameters(acq_params=acq_params)
            #chain.add(p201_node, node)
            chain.add(musst_master, node)

    return (builder, chain)

##########################################################
#
# Display acquired nb points on acquisition devices during
# continuous scan
#
#######################################################################
#####
##### Display acquired nb points on acquisition devices during
##### continuous scan
#####
class ScanOutput(DataWatchCallback):

    STATE_MSG = {
        ScanState.PREPARING: "Preparing",
        ScanState.STARTING: "Running",
        ScanState.STOPPING: "Stopping",
    }

    def __init__(self, musst, points, motor, builder):

        super().__init__()

        self.nb_points = points

        self.musst_time = 0.0
        self.musst = musst
        self.musst_time_factor = 1.0
        self.musst_time_channel = "No Time Channel"
        if musst is not None:
            self.musst_time_channel = f"{self.musst.name}:musst_timer"
            self.musst_time_factor = float(self.musst.get_timer_factor())

        if isinstance(motor, Monochromator):
            self.motor = motor._motors["virtual_energy"]
            self._motor_name = "Energy"
        else:
            self.motor = motor
            if motor is not None:
                self._motor_name = motor.name
            else:
                self._motor_name = "NoneTimeSCAN"

        self.controllers = {}
        if builder is not None:
            for cont_node in builder.get_top_level_nodes():
                #   breakpoint()
                self.controllers[cont_node.controller.name] = {}
                self.controllers[cont_node.controller.name][
                    "name"
                ] = cont_node.controller.name
                self.controllers[cont_node.controller.name]["updated"] = False
                self.controllers[cont_node.controller.name]["points"] = 0
        self.end_scan_time = None

        if self.motor is not None:
            self._last_position = self.motor.position
        else:
            self._last_position = 0.0

        # print(self.controllers)

    def on_scan_new(self, scan, scan_info):

        title = scan_info["title"]
        print(f"\n  {title}\n")

        if self.motor is not None:
            display_str = "   %8s " % (self._motor_name)
        else:
            display_str = "  "

        if self.musst is not None:
            display_str += " %8s " % ("time")

        for cont_name in self.controllers:
            display_str += " %8s " % (self.controllers[cont_name]["name"])

        print(display_str)

    def on_state(self, state):
        return True

    def on_scan_data(self, data_events, nodes, info):
        #print(f"\nENTER ScanOutput on_scan_data - {len(data_events)}")
        if self.motor is not None:
            display_str = "   %8.4f " % (self._last_position)
        else:
            display_str = "  "

        if self.motor is not None:
            self._last_position = self.motor.position
            display_str = "   %8.4f " % (self._last_position)
        else:
            display_str = "  "

        if len(data_events) >= 1:

            for cont_name in self.controllers:
                self.controllers[cont_name]["updated"] = False

            for channel, events in data_events.items():

                data_node = nodes.get(channel)

                #print("\n-------------------------------------------")
                #print(f"{data_node.name}")
                #print(f"{data_node.parent.name}")
                #print(f"{data_node.parent.parent.name}")
                #is_inst = isinstance(data_node, DataNodeContainer)
                #print(f"IS INSTANCE: {is_inst}")
                if not isinstance(data_node, DataNodeContainer):

                    if data_node.name.lower() == self.musst_time_channel:
                        data = data_node.get(-1)
                        self.musst_time = float(data) / float(self.musst_time_factor)
                        cont_name = ""
                        nbp = len(data_node)
                    else:
                        cont_name = data_node.name
                        if cont_name not in self.controllers:
                            cont_name = data_node.parent.name
                            if cont_name not in self.controllers:
                                cont_name = data_node.parent.parent.name

                    if cont_name in self.controllers:
                        cont_obj = self.controllers[cont_name]
                        if not cont_obj["updated"]:
                            cont_obj["updated"] = True
                            #new_len = len(data_node)
                            #old_len = cont_obj["points"]
                            #if new_len != old_len:
                            #    print(f"{data_node.name} - {new_len} - {old_len}")
                            cont_obj["points"] = len(data_node)

        if self.musst is not None:
            display_str += f" {self.musst_time:8.3f} "

        for cont_obj in self.controllers.values():
            display_str += "     %04d " % (cont_obj["points"])

        print(f"{display_str}", end="\r")

        #print(f"LEAVE ScanOutput on_scan_data")

        #sys.stdout.flush()

    def on_scan_end(self, *args):
        print("\n")

class ScanWatchdog(WatchdogCallback):

    STATE_MSG = {
        ScanState.PREPARING: "Preparing",
        ScanState.STARTING: "Running",
        ScanState.STOPPING: "Stopping",
    }

    def __init__(self, musst, points, motor, builder, timeout=5.0):

        super().__init__(watchdog_timeout=timeout)

        self.nb_points = points

        self.musst_time = 0.0
        self.musst = musst
        self.musst_time_factor = 1.0
        self.musst_time_channel = "No Time Channel"
        if musst is not None:
            self.musst_time_channel = f"{self.musst.name}:musst_timer"
            self.musst_time_factor = float(self.musst.get_timer_factor())

        if isinstance(motor, Monochromator):
            self.motor = motor._motors["trajectory"]
        else:
            self.motor = motor

        self.controllers = {}
        if builder is not None:
            for cont_node in builder.get_top_level_nodes():
                self.controllers[cont_node.controller.name] = {}
                self.controllers[cont_node.controller.name][
                    "name"
                ] = cont_node.controller.name
                self.controllers[cont_node.controller.name]["updated"] = False
                self.controllers[cont_node.controller.name]["points"] = 0

        self._timeout_on = False
        self._end_of_scan_reached = False

    def on_scan_new(self, scan, scan_info):
        pass

    def on_state(self, state):
        return True

    def on_scan_data(self, data_events, nodes, info):
        if len(data_events) >= 1:

            self._timeout_on = True

            for cont_name in self.controllers:
                self.controllers[cont_name]["updated"] = False

            for channel, events in data_events.items():

                data_node = nodes.get(channel)

                if not isinstance(data_node, DataNodeContainer):

                    if data_node.name.lower() == self.musst_time_channel:
                        cont_name = ""
                    else:
                        cont_name = data_node.name
                        if cont_name not in self.controllers:
                            cont_name = data_node.parent.name
                            if cont_name not in self.controllers:
                                cont_name = data_node.parent.parent.name

                    if cont_name in self.controllers:
                        cont_obj = self.controllers[cont_name]
                        if not cont_obj["updated"]:
                            cont_obj["updated"] = True
                            cont_obj["points"] = len(data_node)
        #print(f"LEAVE on_scan_data - {len(data_events)}")

    def on_scan_end(self, *args):
        #print("\n ON_SCAN_END\n")
        self._timeout_on = False
        self._end_of_scan_reached = True
        self._time_of_end_of_scan = time.time()

    def on_timeout(self):
        #print("\n TIMEOUT\n")
        if self._end_of_scan_reached:
            ttime = time.time()-self._time_of_end_of_scan
            print(f"\n   {ttime:.3f} since end of scan")
        if self._timeout_on:
            raise TimeoutError("Missing points")
