
import numpy

from bliss.scanning.acquisition.calc import CalcChannelAcquisitionSlave, CalcHook
from bliss.scanning.chain import AcquisitionChannel
from bliss.common.utils import RED, GREEN
from bliss.controllers.bliss_controller import BlissController
from bliss.controllers.counter import CounterController, CalcCounterController

class MusstMonochromatorEncCalcChannelBase:
    """
    MUSST helper to calculate counters from yml configuration
    """
    def __init__(self, name, config):
        self.config = config
        self.name = name
        self.mono = config.get("mono", None)
        self.musst = config.get("musst", None)
        if self.musst is None:
            raise RuntimeError(f"MusstMonochromatorEncCalcChannel ({self.name}): No musst defined")
        self.musst_ch_name = config.get("musst_ch_name", None)
        if self.musst_ch_name is None:
            raise RuntimeError(f"MusstMonochromatorEncCalcChannel ({self.name}): No musst channel name defined")
        self.steps_per_unit = config.get("steps_per_unit", 1)
        self.offset = config.get("offset", 0)

    def get_acquisition_slave(self, musst_acq, scan_par):
        self.scan_par = scan_par
        hook = MusstBaseCalc(
            self.musst,
            self.musst_ch_name,
            self.name,
            self.calculate,
            self.prepare,
        )
        slave = CalcChannelAcquisitionSlave(
            hook.dest_name,
            (musst_acq,),
            hook,
            hook.acquisition_channels
        )
        return slave

    def calculate(self, data):
        raise NotImplemented("Implement calculate method in {0}".format(self.__class__))

    def prepare(self):
        pass

class MusstBaseCalc(CalcHook):
    """
    MUSST helper to calculate counters during continuous scans
    """
    def __init__(
        self,
        master,
        src_channel_name,
        dest_channel_name,
        calculate_func = None,
        prepare_func = None,
        data_per_point=1
    ):
        self.master = master
        self.name = master.name
        self.src_channel_name = src_channel_name
        self.dest_channel_name = dest_channel_name
        self.data_per_point = data_per_point
        if calculate_func is None:
            raise RuntimeError("No calculate function")
        self.calculate_func = calculate_func
        self.prepare_func = prepare_func
        self.__overflow = 0
        self.__last_data = None

    @property
    def dest_name(self):
        return f"{self.name}:{self.dest_channel_name}"

    def compute(self, sender, data_dict):
        data = data_dict.get(self.src_channel_name)

        if data is None:
            return dict()

        calc_data = self.calculate(data)
        if calc_data is not None:
            return { self.dest_name: calc_data }
        else:
            return dict()

    def absolute(self, data):
        if self.__last_data is None:
            self.__last_data = data[0]
        raw_data = numpy.append(self.__last_data, data)
        abs_data = raw_data.astype(numpy.float64)
        if len(raw_data) >= 2:
            over_idxs = numpy.where(numpy.diff(abs_data) != numpy.diff(raw_data))[0]
            abs_data += self.__overflow * 2**32
            for idx in over_idxs:
                ovr_sign = raw_data[idx] > 0 and 1. or -1.
                abs_data[idx+1:] += ovr_sign * 2**32
                self.__overflow += ovr_sign
        self.__last_data = raw_data[-1:]
        return abs_data[len(self.__last_data):]

    def calculate(self, data):
        abs_data = self.absolute(data)
        return self.calculate_func(data, abs_data)

    def prepare(self):
        if self.prepare_func is not None:
            self.prepare_func()

    @property
    def acquisition_channels(self):
        return [AcquisitionChannel(self.dest_name, numpy.float64, ())]

class MusstEnergyFromBraggCalcCounter(MusstMonochromatorEncCalcChannelBase):
    def calculate(self, data, abs_data):
        pos_step = abs_data + self.offset
        angle = pos_step / float(self.steps_per_unit)
        ene_data = self.mono.bragg2energy(angle)
        return ene_data

class MusstMotorCalcCounter(MusstMonochromatorEncCalcChannelBase):
    def calculate(self, data, abs_data):
        pos_step = abs_data + self.offset
        angle = pos_step / float(self.steps_per_unit)
        return angle

class MusstDiffChannelCalcCounter(MusstMonochromatorEncCalcChannelBase):
    """
    Calculate difference between 2 points from a musst channel.
    """
    def __init__(self, name, config):
        super().__init__(name, config)
        self._first_point = True
        self._last_value = 0.0
        self._extra_point = None

    def prepare(self):
        self._first_point = True
        self._last_value = 0.0
        self._extra_point = None

    def calculate(self, data, abs_data):
        if self._first_point:
            self._first_point = False
            if len(data) > 1:
                prepend = 2*data[0] - data[1]
                diff = numpy.diff(data, prepend=prepend)
            else:
                self._extra_point = data[0]
                return None
        else:
            if self._extra_point is not None:
                data = numpy.concatenate((numpy.array([self._extra_point]), data))
                prepend = 2*data[0] - data[1]
                diff = numpy.diff(data, prepend=prepend)
                self._extra_point = None
            else:
                data = numpy.concatenate((numpy.array([self._last_point]), data))
                diff = numpy.diff(data)
        self._last_point = data[-1]

        return diff
