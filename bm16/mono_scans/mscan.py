
import gevent
import numpy

from bliss import global_map
from bliss.config import settings
from bliss.shell.standard import umv
from bliss.config.static import get_config
from bliss.common.cleanup import cleanup
from bliss.common.logtools import log_debug, log_info

class MonochromatorScans:
    """
    ???
    """
    def __init__(self, name, config):

        # internal parameter
        self._name = name
        self._config = config

        # Monochromator
        self._mono = config.get("_par_mono", None)

        # Musst counters
        self._musst_counters = config.get("_par_musst_counters", None)

        # Approximation
        self._approx = config.get("_par_approx", 1e-3)

        # Continuous scans
        self._contscan = config.get("_par_contscan")
        self._contscan.set_musst_counters(self._musst_counters._musst_counter)
        self._contscan.set_musst_calc_counters(self._musst_counters._musst_calc_counter)

        # Preset
        self._preset = []
        presets_config = config.get("_par_preset", None)
        if presets_config is not None:
            for preset_config in presets_config:
                preset = preset_config.get("preset")
                self._preset.append(preset)

        # shutter
        self._shutter = config.get("_par_shutter", None)
        self._contscan.set_shutter(self._shutter)

        # TODO mcoil management during continous scans. Should be remove from scans
        self._mcoil_disable = False

        # Metadata
        self._metadata = config.get("par_metadata", None)

        self._cst_speed_mode = settings.SimpleSetting("MonochromatorScans_cst_speed_mode", default_value= "Energy")
        self._undulator_master = settings.SimpleSetting("MonochromatorScans_undulator_master", default_value= "")
        global_map.register(self)


    def __info__(self):
        """
        """
        log_debug(self, "MonochromatorScans __init__  >>>>>>>>>>>>>>>>>>>>>>>>")

        return "MonochromatorScans KOHZU"

    """
    Trajectory mode
    """
    @property
    def cst_speed_mode(self):
        return self._cst_speed_mode.get()

    @cst_speed_mode.setter
    def cst_speed_mode(self, val):
        mode = ["energy", "bragg", "undulator"]
        if val in mode:
            self._cst_speed_mode.set(val)
        else:
            mode_str = "/".join(mode)
            raise ValueError(f"{val} is not a correct mode ({mode_str})")

    """
    Undulator master
    """
    @property
    def undulator_master(self):
        if self._undulator_master.get() == "":
            return None
        else:
            return get_config().get(str(self._undulator_master.get()))

    @undulator_master.setter
    def undulator_master(self, undulator):
        self._undulator_master.set(undulator.name)

    """
    HOOK methods
    """
    def _mscan_prepare(self):
        for preset in self._preset:
            preset.prepare(None)

    def _mscan_stop(self):
        for preset in self._preset:
            preset.stop(None)

    def _mscan_cleanup(self):

        # Stop on preset object
        self._mscan_stop()

        # Check mcoil
        if self._mcoil_disable:
            self._motors["trajectory"].enable_axis(axis=self._motors["mcoil"])
            self._mcoil_disable = False
            last_position = self._mono._motors["mbragg"].position + self._mono._bragg_offset
            last_position = self._mono.bragg2energy(last_position)
            umv(self._mono._motors["energy_tracker"], last_position)

    """
    Continuous scan in time
    No Motor Movement
    """
    def time(self, nb_points, time_per_point, *counters, comment=None):
        self._contscan._scan("time", None, 0, 0, nb_points, time_per_point, "TIME", "TIME", *counters, comment=comment)


    def energy(self, Estart, Estop, nb_points, time_per_point, *counters, nbscan=1, backNforth=False, comment=None):

        if numpy.isclose(Estart, Estop, atol=self._approx):
            raise RuntimeError(f"MonochromatorScans.energy: start({Estart}) == stop({Estop})  (+/- {self._approx})")

        # Load trajectory to calculate max_spee/max_acc
        if Estart < Estop:
            traj_start = Estart
            traj_stop = Estop
        else:
            traj_start = Estop
            traj_stop = Estart

        self._mono._load_trajectory(
            traj_start,
            traj_stop,
            self.cst_speed_mode,
            self.undulator_master,
            nb_points=nb_points,
            time_per_point=time_per_point
        )

        # Move Trajectory axis to start position
        # Needed to set virtual axis to a "right" position
        if self.cst_speed_mode == "energy":
            traj_start = Estart
        elif self.cst_speed_mode == "bragg":
            traj_start = self._mono.energy2bragg(Estart)
        elif self.cst_speed_mode == "undulator":
            traj_start = self._mono.undulator_master.tracking.energy2tracker(Estart)

        if hasattr(self._mono, "_mcoil_move"):
            if not self._mono._mcoil_move:
                mcoil_start = self._energy2bragg(Estart) - self._mono._bragg_offset
                mcoil_stop = self._energy2bragg(Estop) - self._mono._bragg_offset
                if numpy.abs(mcoil_stop - mcoil_start) < self._mono._max_coil_range:
                    # move Edcm to middle position of the scan
                    umv(self._mono._motors["energy_tracker"], (Estart+Estop)/2.0)
                    # move Trajectory to middle position of the scan
                    self._mono._motors["virtual_energy"].position = (Estart+Estop)/2.0
                    self._mono._motors["virtual_energy"].offset=0
                    umv(self._mono._motors["trajectory"], (Estart+Estop)/2.0)
                    # disble mcoil from trajectory
                    self._motors["trajectory"].disable_axis(axis=self._motors["mcoil"])
                    # set flag mcoil_disable to true to be enable mcoil at the end of the scan
                    self._mcoil_disable = True
                else:
                    print(RED("Force MCOIL move"))

        self._mono._motors["virtual_energy"].position = Estart
        self._mono._motors["virtual_energy"].offset=0
        umv(self._mono._motors["trajectory"], traj_start)

        # Prepare on preset object
        self._mscan_prepare()

        # INIT VARIABLES
        if Estart > Estop:
            way = "down"
            way_back = "up"
        else:
            way = "up"
            way_back = "down"

        for nscan in range(nbscan):

            # SCAN PARAMETERS
            if not backNforth or (backNforth and nscan%2==0):
                start_pos = Estart
                stop_pos = Estop
                s_way = way
            else:
                start_pos = Estop
                stop_pos = Estart
                s_way = way_back

            print(f"\nSCAN #{nscan+1}(/{nbscan}) - {s_way.upper()} direction")
            self._contscan._scan(
                "EnergyCont",
                self._mono,
                start_pos,
                stop_pos,
                nb_points,
                time_per_point,
                *counters,
                trajectory_mode=self.cst_speed_mode,
                undulator_master=self.undulator_master,
                comment=comment,
            )
