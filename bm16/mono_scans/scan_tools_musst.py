
import numpy

from bliss.config import settings
from bliss.scanning.acquisition.musst import MusstAcquisitionMaster
from bliss.controllers.motors.icepap.trajectory import TrajectoryAxis
from bliss.controllers.monochromator import Monochromator

#######################################################################
#####
##### MUSST helper to get channel configured type
#####
def musst_get_channel_type(musst, ch_num):
    cmd = f"?CHCFG CH{ch_num}"
    return musst.putget(cmd)

#######################################################################
#####
##### MUSST helper to get AcquisitionMaster
#####
def musst_get_master(musst, scan_par, channel_list):
    """
    Input Params.
        - musst: Bliss object which synchronize the scan
        - scan_par = {}
            "start"        : start position of the scan
            "stop"         : stop position of the scan
            "points"       : Number of points in the scan
            "time"         : Integration time(s) pwer points
            "trig_start"   : type of synchronization ("TIME"/"POSITION")
                             for start point
            "trig_point"   : type of synchronization ("TIME"/"POSITION")
                             for all other points
        - channel_list     : MUSST channel number to be saved
    """
    
    if isinstance(scan_par["motor"], TrajectoryAxis):
        motor = scan_par["motor"].controller.axes["mtraj"]
    else:
        motor = scan_par["motor"]
    
    # MUSST: TO BE REMOVED (WORKAROUND)
    musst.ABORT
    musst.CLEAR
    
    # MUSST: memory configuration
    musst.set_histogram_buffer_size(2048, 1)  # Histogram (MCA)
    musst.set_event_buffer_size(int(524288 / 16), 1)  # Buffers
    
    # MUSST: channel(s) to be read
    store_mask = 0
    for chan in channel_list:
        store_mask |= 1 << int(chan)
    
    # zap time
    if motor is None:
        schan = 0
        sdata = int(0.001 * musst.get_timer_factor())
        pchan = 0
        pdata = int(scan_par["time"] * musst.get_timer_factor())
        pscandir = 0
        sscandir = 0
    # zap with movement
    else:
        # Intitialize trigged channel to current position
        if isinstance(motor, Monochromator):
            mot_name = motor._motors["virtual_energy"].name
            steps_per_unit = motor._motors["virtual_energy"].steps_per_unit
            if scan_par["trajectory_mode"] == "energy":
                motpos = motor._motors["trajectory"].position * steps_per_unit
            elif scan_par["trajectory_mode"] == "bragg":
                motpos = motor.bragg2energy(motor._motors["trajectory"].position) * steps_per_unit
            elif scan_par["trajectory_mode"] == "undulator":
                motor._motors["bragg_rotation"].sync_hard()
                motpos = motor.bragg2energy(motor._motors["bragg_rotation"].position + motor._bragg_offset.get()) * steps_per_unit

                #motpos = motor.undulator_master.tracking.tracker2energy(motor._motors["trajectory"].position) * steps_per_unit
                #scan_par["start"] = motor.undulator_master.tracking.tracker2energy(motor.undulator_master.tracking.energy2tracker(scan_par["start"]))
                #scan_par["stop"] = motor.undulator_master.tracking.tracker2energy(motor.undulator_master.tracking.energy2tracker(scan_par["stop"]))
            else:
                raise ValueError("Invalid Constant Speed Mode")
        else:
            mot_name = motor.name
            steps_per_unit = motor.steps_per_unit
            motpos = motor.position * motor.steps_per_unit
        #channel = int(musst.counters[mot_name].channel[-1])
        channel = int(musst.get_channel_by_name(mot_name).channel_id)
        channel_str = f"CH{channel}"
        musst.putget(f"CH {channel_str} {motpos}")
        
        # Start tigged on time
        if scan_par["trig_start"] == "TIME":
            schan = 0
            sdata = int(scan_par["time"] * musst.get_timer_factor())
            sscandir = 0
        # start trigged on position
        else:
            schan = channel
            sdata = int(scan_par["start"] * steps_per_unit)
            if scan_par["stop"] > scan_par["start"]:
                sscandir = 0
            else:
                sscandir = 1
        # point trigged on time
        if scan_par["trig_point"] == "TIME":
            pchan = 0
            pdata = int(scan_par["time"] * musst.get_timer_factor())
            pscandir = 0
        # point trigged on position
        else:
            pchan = channel
            delta = abs(scan_par["stop"] - scan_par["start"]) / scan_par["points"]
            pdata = int(delta * steps_per_unit)
            if scan_par["stop"] > scan_par["start"]:
                pscandir = 0
            else:
                pscandir = 1
                
    # MUSST master parameters
    musst_vars = {
        "SMODE": 1,  # 0=external trigger / 1=internal channel
        "SCHAN": schan,  # if SMODE=1 0=time 1=ch1 2=ch2 3=ch3 4=ch4 5=ch5 6=ch6
        "SDATA": sdata,
        "PMODE": 1,
        "PCHAN": pchan,
        "PDATA": pdata,
        "NPOINT": int(scan_par["points"]),
        "PSCANDIR": pscandir,  # 0=positive direction 1=negative direction
        "SSCANDIR": sscandir,  # 0=positive direction 1=negative direction
        "STOREMSK": store_mask,
    }
    
    # MUSST: acquisition master
    musst_master = MusstAcquisitionMaster(
        musst,
        program="zapintgen.mprg",
        program_start_name="HOOK",
        vars=musst_vars,
    )

    return musst_master
