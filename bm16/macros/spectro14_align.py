#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Alignment macros for the BM16 X-ray emission spectrometer (a.k.a. `spectro14`)
===============================================================================
"""
# import an axis
from bliss.setup_globals import emi

from bm16.utils.bragg import get_dspacing
from bm16.controllers.spectro14 import calc_pos_com


def setEmi(newemi):
    """Set the emission energy (emi) and resets real motors user values

    Parameters
    ----------
    newemi : float
        new emission energy

    NOTES
    -----
    - pseudo_motor.dial(newvalue) -> sets only user value of real_motor to
                                     the calculated position
    - pseudo_motor.position(newvalue) -> sets only offset between dial
                                       and user of the pseudo_motor
    - the dial value of real motors has to be resetted with
      real_motor.dial(newal)

    """
    emi.dial(newemi)
    emi.position(newemi)
    _conf = emi.get_conf()
    _new_dict = calc_pos_com(
        newemi,
        d=_conf["CAS_d"],
        r=_conf["CAS_R"],
        dz=_conf["CAS_DZ"],
        sz=_conf["CAS_SZ"],
    )
    for real in emi.controller.reals:
        real.position(_new_dict[real.name])

    print("Resetted emission energy to {0}".format(newemi))


def confspectro(mat, hkl, r=1000.0, dz=82.0, sz=500.0, thmin=65.0, thmax=89.0, d=None):
    """update spectrometer configuration

    Parameters
    ----------
    mat : string
        material ("Si" or "Ge")
    hkl : tuple of integers, (h, k, l)
    r: float
        crystal bending radius in mm [1000.]
    dz : float
        vertical separation of two rows of crystals in mm [82.]
    sz : float
        sample vertical offset from table in mm [500.]
    thmin : float
        spectrometer minimum theta in deg [65.]
    thmax : float
        spectrometer maximum theta in deg [89.]
    d : float
        d-spacing [None] (if none, calculated automatically)

    """
    # check inputs
    if not (mat == "Ge" or mat == "Si"):
        raise NameError('mat: "Ge" or "Si"')
    if not ("tuple" in str(type(hkl)) or len(hkl) == 3):
        raise NameError("hkl: (h, k, l)")
    if r < 950.0 or r > 1050.0:
        raise NameError("r: [950-1050] mm")

    emi.controller.CAS_R = r
    emi.controller.CAS_MAT = mat
    emi.controller.CAS_HKL = hkl
    emi.controller.CAS_DZ = dz
    emi.controller.CAS_SZ = sz
    emi.controller.CAS_THmin = thmin
    emi.controller.CAS_THmax = thmax
    emi.controller.CAS_d = get_dspacing(mat, hkl)
    emi.controller.update_conf()


if __name__ == "__main__":
    pass
