import bliss.common.utils as utils
from bliss.config.static import get_config
from bliss.controllers.tango_shutter import TangoShutterState
from bliss.common.standard import interlock_state
from bliss.setup_globals import *

config = get_config()

T_bl = {}  # Temperatures from the wagos
W_bl = {}  # Interlocks from the wagos
M_bl = {}  # Monochromator cryogenic system


class BL(object):
    """
    Class to check beamline status and to manipulate valves and shutters.
        bl=BL()             : 
        bl                  : return all the status
        bl.ring()           :
        bl.wagointlks()     :
        bl.valves()         : 
        bl.chillers()       : 
        bl.pressures()      : 
        bl.temperatures()   :
        bl.open()           : open frontend and all valves in optics hutch
                              starts monitoring of all valves in beam path 
                              send a warning in case on valves is closed
        bl.close()          : close the frontend
        bl.close_all()      : close the frontend and all the valves  

        # reading values
        bl.SR_Current.value 
        bl.P_M1.value
        ...

        
    """

    try:
        _BLDEV_ring = config.get("BLDEV_ring")["names"].raw_list
    except:
        _BLDEV_ring = list()

    try:
        _BLDEV_wagointlks = list()
        _BLDEV_wago_flag = 0
        for _i in interlock_state():
            _BLDEV_wagointlks.append(_i)
            _BLDEV_wago_flag = "session_wago"
    except:
        _BLDEV_wagointlks = list()
        _BLDEV_wago_flag = 0

    try:
        _BLDEV_valves = config.get("BLDEV_valves")["names"].raw_list
    except:
        _BLDEV_valves = list()
    try:
        _polling_valves = config.get("BLDEV_valves")["polling"]
    except:
        _polling_valves = 3600

    try:
        _BLDEV_pressure = config.get("BLDEV_pressure")["names"].raw_list
    except:
        _BLDEV_pressure = list()

    try:
        _BLDEV_chillers = config.get("BLDEV_chiller")["names"].raw_list
    except:
        _BLDEV_chillers = list()
    try:
        _polling_chil = config.get("BLDEV_chiller")["polling"]
    except:
        _polling_chil = 3600

    try:
        _BLDEV_temperatures = list()
        for _i in Temperatures._config_counters:
            _BLDEV_temperatures.append(_i)
        _BLDEV_temp_flag = "session_wago"
    except:
        _BLDEV_temperatures = list()
        _BLDEV_temp_flag = 0

    try:
        _BLDEV_cryomono = list()
        for _i in MonoCryo._config_counters:
            _BLDEV_cryomono.append(_i)
        _BLDEV_cryomono_flag = "session_wago"
    except:
        _BLDEV_cryomono = list()
        _BLDEV_cryomono_flag = 0

    _BLDEV = (
        ["frontend"]
        + _BLDEV_wagointlks
        + _BLDEV_ring
        + _BLDEV_valves
        + _BLDEV_pressure
        + _BLDEV_chillers
    )

    def __init__(self):
        for i in self._BLDEV:
            setattr(self, i, config.get(i))

        # start chiller monitoring
        self.monitorchillers = utils.periodic_exec(
            self._polling_chil, self.check_chillers
        )
        self.monitorchillers.__enter__()

        self.monitor_valves = utils.periodic_exec(
            self._polling_valves, self.check_valves
        )
        self.monitor_valves.running = False

    def __info__(self):
        s = list()
        s.append("--- FRONTEND ---")
        s.append(self.frontend.__info__())
        s_wagointlks = self._wagointlks()
        if s_wagointlks:
            s.append("\n\n--- WAGO INTERLOCKS ---")
            s.append(s_wagointlks)
        s.append("\n\n--- RING ---")
        s.append(self._ring())
        s.append("\n\n--- VALVES ---")
        s.append(self._valves())
        s.append("\n\n--- PRESSURES ---")
        s.append(self._pressures())
        s_chillers = self._chillers()
        if s_chillers:
            s.append("\n\n--- CHILLERS ---")
            s.append(s_chillers)
        s_temperatures = self._temperatures()
        if s_temperatures:
            s.append("\n\n--- TEMPERATURES ---")
            s.append(s_temperatures)
        s_cryomono = self._cryomono()
        if s_cryomono:
            s.append("\n\n--- MONOCHROMATOR ---")
            s.append(s_cryomono)
        return "\n".join(s)

    def pressures(self):
        print(self._pressures())

    def _pressures(self):
        s = list()
        max_len = max([len(name) for name in self._BLDEV_pressure], default=0)
        for i in self._BLDEV_pressure:
            jauge = config._name2instance[i]
            try:
                s.append(f"{i:{max_len}s} : {jauge.value:7.1e} {jauge.unit}")
            except:
                s.append(f"{i:{max_len}s} : FAULT")
        return "\n".join(s)

    def _temperatures(self):
        s = list()
        if self._BLDEV_temp_flag == "session_wago":
            max_len = max([len(name) for name in self._BLDEV_temperatures], default=0)
            for i in self._BLDEV_temperatures:
                tc = globals()[i]
                s.append(f"{i:{max_len}s} : {tc.raw_read} degrees")
        else:
            max_len = max([len(name) for name in T_bl], default=0)
            for i in T_bl:
                s.append(f"{i:{max_len}s} : {T_bl[i]} degrees")

        return "\n".join(s)

    def temperatures(self):
        print(self._temperatures())

    def _cryomono(self):
        s = list()
        if self._BLDEV_cryomono_flag == "session_wago":
            max_len = max([len(name) for name in self._BLDEV_cryomono], default=0)
            s.append(
                f"{'euro':{max_len}s} : {euromono.read_all(euromono.counters[0])[0]:.2f} degrees"
            )
            s.append(
                f"{'outp':{max_len}s} : {euromono.read_all(euromono.counters[1])[0]:.2f} %"
            )
            s.append(
                f"{'VR':{max_len}s} : {euromono.read_all(euromono.counters[2])[0]:.2f} %"
            )
            s.append(
                f"{'ln2':{max_len}s} : {s7mono.read_all(s7mono.counters[0])[0]:.2f} %"
            )
            s.append(
                f"{'pln2':{max_len}s} : {s7mono.read_all(s7mono.counters[1])[0]:.2f} bar"
            )

        else:
            max_len = max([len(name) for name in M_bl], default=0)
            s.append(f"{'euro':{max_len}s} : {M_bl['T_euro']} degrees")
            s.append(f"{'outp':{max_len}s} : {M_bl['outP']} %")
            s.append(f"{'VR':{max_len}s} : {M_bl['VR']} %")
            s.append(f"{'ln2':{max_len}s} : {M_bl['ln2']} %")
            s.append(f"{'pln2':{max_len}s} : {M_bl['pln2']} bar")

        return "\n".join(s)

    def cryomono(self):
        print(self._cryomono())

    def _wagointlks(self):
        s = list()
        if self._BLDEV_wago_flag == "session_wago":
            intlkstate = interlock_state()
            for i in self._BLDEV_wagointlks:
                if intlkstate[i][0].tripped:
                    s.append(f"{i} : TRIPPED")
                else:
                    s.append(f"{i} : NOT TRIPPED")
        else:
            for i in W_bl:
                if W_bl[i] == True:
                    s.append(f"{i} : TRIPPED")
                elif W_bl[i] == False:
                    s.append(f"{i} : NOT TRIPPED")
                else:
                    s.append(f"{i} : {W_bl[i]}")

        return "\n".join(s)

    def wagointlks(self):
        print(self._wagointlks())

    def chillers(self):
        print(self._chillers())

    def _chillers(self):
        s = list()
        for i in self._BLDEV_chillers:
            s.append(f"\n- {i} -")
            s.append(f"{config._name2instance[i].__info__()}")
        return "\n".join(s)

    def valves(self):
        print(self._valves())

    def _valves(self):
        s = list()
        max_len = max([len(name) for name in self._BLDEV_valves], default=0)
        for i in self._BLDEV_valves:
            s.append(f"{i:{max_len}s} : {config._name2instance[i].__info__()}")
        return "\n".join(s)

    def ring(self):
        print(self._ring())

    def _ring(self):
        s = list()
        max_len = max([len(name) for name in self._BLDEV_ring], default=0)
        for i in self._BLDEV_ring:
            v = config._name2instance[i]
            if v.unit:
                s.append(
                    f"{i:{max_len}} : {v.raw_value:{v.format_string.lstrip('%')}} {v.unit}"
                )
            else:
                s.append(f"{i:{max_len}} : {v.raw_value:{v.format_string.lstrip('%')}}")
        return "\n".join(s)

    def open(self):
        """
        FIRST open the beamline valves, THEN the frontend
        """
        no_error = True
        for v in self._BLDEV_valves + ["frontend"]:
            valve = config._name2instance[v]
            status = valve.__info__().upper()
            TANGO_status = valve.state
            if TANGO_status is TangoShutterState.FAULT:
                print(f"## ERROR ## fault on {v}")
                no_error = False
            elif "INTERLOCK" in status:
                print(f"## ERROR ## interlock on {v}")
                no_error = False
            elif TANGO_status is not TangoShutterState.OPEN:
                if "MANUAL" in status:
                    print(f"### WARNING ### {v} : {status}")
                else:
                    print(f"Opening {v}...")
                    valve.open()
                    if valve.state is not TangoShutterState.OPEN:
                        print(f"## ERROR ## cannot open {v} : {valve.__info__()}")
                        no_error = False
            else:
                print(f"{v} : {TANGO_status.value}")
        if no_error:
            print("Starting beamline monitoring")
            self.monitor_valves.__enter__()
            self.monitor_valves.running = True

    def close_all(self):
        """ 
        FIRST close the frontend, THEN the beamline valves
        """
        for v in ["frontend"] + self._BLDEV_valves:
            valve = config._name2instance[v]
            status = valve.__info__().upper()
            TANGO_status = valve.state
            if TANGO_status is TangoShutterState.FAULT:
                print(f"## ERROR ## fault on {v}")
            elif "INTERLOCK" in status:
                print(f"## ERROR ## interlock on {v}")
            elif TANGO_status is not TangoShutterState.CLOSED:
                if "MANUAL" in status:
                    print(f"### WARNING ### {v} : {status}")
                else:
                    print(f"Closing {v}...")
                    valve.close()
                    if valve.state is not TangoShutterState.CLOSED:
                        print(f"## ERROR ## cannot close {v} : {valve.__info__()}")
            else:
                print(f"{v} : {TANGO_status.value}")
        if self.monitor_valves.running:
            print("Stopping beamline monitoring")
            self.monitor_valves.__exit__()
            self.monitor_valves.running = False

    def close(self):
        """
        Close only the frontend
        """
        TANGO_status = self.frontend.state
        if TANGO_status is TangoShutterState.OPEN:
            print("Closing frontend...")
            self.frontend.close()
        else:
            print(f"## WARNING ## - {TANGO_status.value}")
            print(self.frontend.__info__())
        if self.monitor_valves.running:
            print("Stopping beamline monitoring")
            self.monitor_valves.__exit__()
            self.monitor_valves.running = False

    def check_valves(self):
        for v in self._BLDEV_valves + ["frontend"]:
            TANGO_status = config._name2instance[v].state
            if TANGO_status is not TangoShutterState.OPEN:
                print(f"## WARNING ## {v} : {TANGO_status.name}")

    def check_chillers(self):
        for c in self._BLDEV_chillers:
            config._name2instance[c].read_status()
