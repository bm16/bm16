from bliss.setup_globals import params
from collections import namedtuple


def add_params_to_globals():
    """Add `params` object to global variables"""
    for i in params["device"]:
        tmp = i.to_dict()
        typedevice = namedtuple(i["name"], tmp.keys())
        globals()[i["name"]] = typedevice(*tmp.values())
