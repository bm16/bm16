import time
from bliss.config.static import get_config

config = get_config()
dsp7230 = config.get('dsp7230')
dac = config.get('dac')
regul_mono = config.get('regul_mono')
regul_mono.history_size = 1000
regul_mono.setpoint = 0

regul_plot = regul_mono.plot()
regul_plot.sleep_time = 0.5
regul_plot.fig.xlabel = "Red:mag, Green:dac, Blue:deadband/setpoint"

# Limits and increment value of all the useful variables
DAC_HLIM = 10  # V
DAC_LLIM = 0


def feedbackon():
    """start monochromator feedback"""
    sp = regul_mono.setpoint
    regul_mono.setpoint = sp
    assert regul_mono.is_regulating()
    regul_plot.start()


def feedbackoff():
    """stop monochromator feedback"""
    regul_mono._stop_regulation()
    regul_plot.stop()
    assert not regul_mono.is_regulating()


def piezo_value(Val_0_10V):
    if DAC_LLIM <= Val_0_10V <= DAC_HLIM:
        dsp7230.io_write("DAC. 1 " + str(Val_0_10V))
        time.sleep(0.1)
        dac.sync_hard()
        print("New DAC value: {}".format(dsp7230.get_formated_info()["DAC"]))
    else:
        print("DAC must be in the range {}-{}V.".format(DAC_LLIM, DAC_HLIM))


def piezo_increase(Increment_in_V=0.1):
    val = float(dsp7230.get_formated_info()["DAC"]) + Increment_in_V
    if DAC_LLIM <= val <= DAC_HLIM:
        dsp7230.io_write("DAC. 1 " + str("{:.3f}".format(round(val, 3))))
        time.sleep(0.1)
        dac.sync_hard()
        print("New DAC value: {}".format(dsp7230.get_formated_info()["DAC"]))
    else:
        print("DAC high limit...")


def piezo_decrease(Increment_in_V=0.1):
    val = float(dsp7230.get_formated_info()["DAC"]) - Increment_in_V
    if DAC_LLIM <= val <= DAC_HLIM:
        dsp7230.io_write("DAC. 1 " + str("{:.3f}".format(round(val, 3))))
        time.sleep(0.1)
        dac.sync_hard()
        print("New DAC value: {}".format(dsp7230.get_formated_info()["DAC"]))
    else:
        print("DAC low limit...")


def wpiezo(**kwargs):
    """
    Displays the values of the piezo regulation
    """
    print(
        "{:^20}-{:^20}".format(
            "DAC = " + dsp7230.get_formated_info()["DAC"] + " V",
            "P = " + str(regul_mono.kp),
        )
    )
    print(
        "{:^20}-{:^20}-{:^20}".format(
            "Fsamp = " + str(regul_mono.sampling_frequency) + " Hz",
            "Aosc = " + dsp7230.get_formated_info()["Aosc"] + " V",
            "Fosc = " + dsp7230.get_formated_info()["Fosc"] + " Hz",
        )
    )
    print(
        "{:^20}-{:^20}-{:^20}".format(
            "DeadBand = " + str(regul_mono.deadband),
            "Sens = " + dsp7230.get_formated_info()["Sens"],
            "TimeC = " + dsp7230.get_formated_info()["TimeC"],
        )
    )
    print("\nUseful commands:")
    print("-- To start the regulation        --> feedbackon")
    print("-- To stop the regulation         --> feedbackoff")
    print(
        "-- To change P, Fsamp, DeadBand   --> regul_mono.kp, regul_mono.sampling_frequency, regul_mono.deadband"
    )
    print(
        "-- To change DAC                  --> piezo_value, piezo_increase, piezo_decrease"
    )
    print("-- To change Fosc, Aosc           --> dsp7230.set_Fosc, dsp7230.set_Aosc")
    print(
        "-- To change Sens, TimeC          --> dsp7230.increase(decrease)_Sens(TimeC)"
    )


"""
TODO: remove ???
#Premiers essais

P_HLIM=10
P_LLIM=0
P_INC=0.01

Fsamp_HLIM=10 #Hz
Fsamp_LLIM=0

FOSC_HLIM=500 #Hz
FOSC_LLIM=0.001 #Hz

AOSC_HLIM=0.2 #V
AOSC_LLIM=0
AOSC_INC=0.001 #V

SENS_HLIM=27
SENS_LLIM=3

TC_HLIM=30
TC_LLIM=0

def piezo_change(var, val):
    '''To change the value of DAC, P, Fsamp, Aosc, Fosc or DeadBand.'''
    feedback = regul_mono
    if var in ("Sens", "TimeC"):
        print("For Sens and TimeC, use piezo_adjust")

    elif str(var)=="DAC":
        piezo_state=feedback.is_regulating()
        feedback.stop()
        if DAC_LLIM<=val<=DAC_HLIM:
            dsp7230.io_write("DAC. 1 "+str(val))
            time.sleep(0.1)
            dac.sync_hard()
            print("New DAC value: {}".format(dsp7230.get_formated_info()['DAC']))
        else:
            print("DAC must be in the range {}-{}V.".format(DAC_LLIM,DAC_HLIM))
        if piezo_state:
            feedback.setpoint=0

    elif var=="P":
        regul_mono.kp=val
        print("New P value: {}".format(regul_mono.kp))

    elif var=="Fsamp":
        if Fsamp_LLIM<=val<=Fsamp_HLIM:
            regul_mono.sampling_frequency=val
            print("New sampling frequency: {}".format(regul_mono.sampling_frequency))
        else:
            print("Sampling frequency must be in the range {}-{}Hz.".format(Fsamp_LLIM,Fsamp_HLIM))

    elif str(var)=="Aosc":
        if AOSC_LLIM<=val<=AOSC_HLIM:
            dsp7230.io_write("OA. "+str(val))
            time.sleep(0.1)
            print("New Aosc value: {}".format(dsp7230.get_formated_info()['Aosc']))
        else:
            print("Aosc must be in the range {}-{}V.".format(AOSC_LLIM,AOSC_HLIM))

    elif str(var)=="Fosc":
        if FOSC_LLIM<=val<=FOSC_HLIM:
            dsp7230.io_write("OF. "+str(val))
            time.sleep(0.1)
            print("New Fosc value: {}".format(dsp7230.get_formated_info()['Fosc']))
            freq.sync_hard()
        else:
            print("Fosc must be in the range {}-{}Hz.".format(FOSC_LLIM,FOSC_HLIM))

    elif var=="DeadBand":
        regul_mono.deadband=val
        print("New DeadBand value: {}".format(regul_mono.deadband))

    else:
        print("INVALID VARIABLE: choose in DAC, P, Fsamp, Aosc and Fosc.")

def piezo_adjust(var):
    '''To increment the value of DAC by DAC_INC, P by P_INC or Aosc by AOSC_INC.
       Or to change the Sens or TimeC value according to the conversion table.'''
    feedback = regul_mono
    if var in ("Fsamp", "Fosc", "DeadBand"):
        print("For Fsamp, Fosc and DeadBand, use piezo_change")

    elif str(var)=="DAC":
        piezo_state=feedback.is_regulating()
        feedback.stop()
        cmd="+"
        while(1):
            cmd=str(input("+/- or q ? {}".format(cmd)) or cmd)
            if cmd=="+":
                val=float(dsp7230.get_formated_info()['DAC'])+DAC_INC
                if DAC_LLIM<=val<=DAC_HLIM:
                    dsp7230.io_write("DAC. 1 "+str("{:.3f}".format(round(val, 3))))
                    time.sleep(0.1)
                    print("New DAC value: {}".format(dsp7230.get_formated_info()['DAC']))
                else:
                    print("DAC high limit...")
            elif cmd=="-":
                val=float(dsp7230.get_formated_info()['DAC'])-DAC_INC
                if DAC_LLIM<=val<=DAC_HLIM:
                    dsp7230.io_write("DAC. 1 "+str("{:.3f}".format(round(val, 3))))
                    time.sleep(0.1)
                    print("New DAC value: {}".format(dsp7230.get_formated_info()['DAC']))
                else:
                    print("DAC low limit...")
            else:
                print("End of adjustment. Current DAC value: {} V.".format(dsp7230.get_formated_info()['DAC']))
                dac.sync_hard()
                break
        if piezo_state:
            feedback.setpoint=0
                
    elif str(var)=="P":
        cmd="+"
        while(1):
            cmd=str(input("+/- or q ? {}".format(cmd)) or cmd)
            if cmd=="+":
                val=float(regul_mono.kp)+P_INC
                if P_LLIM<=val<=P_HLIM:
                    regul_mono.kp=val
                    print("New P value: {}".format(regul_mono.kp))
                else:
                    print("P high limit...")
            elif cmd=="-":
                val=float(regul_mono.kp)-P_INC
                if P_LLIM<=val<=P_HLIM:
                    regul_mono.kp=val
                    print("New P value: {}".format(regul_mono.kp))
                else:
                    print("P low limit...")
            else:
                print("End of adjustment. Current P value: {}.".format(regul_mono.kp))
                break
                
    elif str(var)=="Aosc":
        cmd="+"
        while(1):
            cmd=str(input("+/- or q ? {}".format(cmd)) or cmd)
            if cmd=="+":
                val=float(dsp7230.get_formated_info()['Aosc'])+AOSC_INC
                if AOSC_LLIM<=val<=AOSC_HLIM:
                    dsp7230.io_write("OA. "+str("{:.4f}".format(round(val, 4))))
                    time.sleep(0.3)
                    print("New Aosc value: {}".format(dsp7230.get_formated_info()['Aosc']))
                else:
                    print("Aosc high limit...")
            elif cmd=="-":
                val=float(dsp7230.get_formated_info()['Aosc'])-AOSC_INC
                if AOSC_LLIM<=val<=AOSC_HLIM:
                    dsp7230.io_write("OA. "+str("{:.4f}".format(round(val, 4))))
                    time.sleep(0.3)
                    print("New Aosc value: {}".format(dsp7230.get_formated_info()['Aosc']))
                else:
                    print("Aosc low limit...")
            else:
                print("End of adjustment. Current Aosc value: {} V.".format(dsp7230.get_formated_info()['Aosc']))
                break
                
    elif str(var)=="Sens":
        cmd="+"
        while(1):
            cmd=str(input("+/- or q ? {}".format(cmd)) or cmd)
            if cmd=="+":
                val=int(dsp7230.io_read("SEN"))+1
                if SENS_LLIM<=val<=SENS_HLIM:
                    dsp7230.io_write("SEN "+str(val))
                    time.sleep(0.3)
                    print("New Sens value: {}".format(dsp7230.get_formated_info()['Sens']))
                else:
                    print("Sens high limit...")
            elif cmd=="-":
                val=int(dsp7230.io_read("SEN"))-1
                if SENS_LLIM<=val<=SENS_HLIM:
                    dsp7230.io_write("SEN "+str(val))
                    time.sleep(0.3)
                    print("New Sens value: {}".format(dsp7230.get_formated_info()['Sens']))
                else:
                    print("Sens low limit...")
            else:
                print("End of adjustment. Current Sens value: {}.".format(dsp7230.get_formated_info()['Sens']))
                break
                
    elif str(var)=="TimeC":
        cmd="+"
        while(1):
            cmd=str(input("+/- or q ? {}".format(cmd)) or cmd)
            if cmd=="+":
                val=int(dsp7230.io_read("TC"))+1
                if TC_LLIM<=val<=TC_HLIM:
                    dsp7230.io_write("TC "+str(val))
                    time.sleep(0.3)
                    print("New TimeC value: {}".format(dsp7230.get_formated_info()['TimeC']))
                else:
                    print("TimeC high limit...")
            elif cmd=="-":
                val=int(dsp7230.io_read("TC"))-1
                if TC_LLIM<=val<=TC_HLIM:
                    dsp7230.io_write("TC "+str(val))
                    time.sleep(0.3)
                    print("New TimeC value: {}".format(dsp7230.get_formated_info()['TimeC']))
                else:
                    print("TimeC low limit...")
            else:
                print("End of adjustment. Current TimeC value: {}.".format(dsp7230.get_formated_info()['TimeC']))
                break

    else:
        print("INVALID VARIABLE: choose in DAC, P, Aosc, Sens and TimeC.")
"""
