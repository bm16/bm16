from gevent import sleep
from bliss.config.static import get_config

_conf = get_config()
BSH = _conf.get('BSH')


def shop():
    print("Opening the beam shutter...")
    while BSH._tango_state == "DISABLE":
        if BSH.state_string[0] == "Hutch not searched":
            print("WARNING: Hutch not searched")
        sleep(1)
    BSH.open()


def shcl():
    print("Closing the beam shutter...")
    if BSH.state_string[0] == "Hutch not searched":
        print("WARNING: Hutch not searched / shutter is closed")
    else:
        BSH.close()
