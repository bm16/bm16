from bliss.config.channels import Channel
from bliss.common.counter import SoftCounter

picB1_cnt = SoftCounter(Channel("picB_ch1"))
picB2_cnt = SoftCounter(Channel("picB_ch2"))
picB3_cnt = SoftCounter(Channel("picB_ch3"))
picB4_cnt = SoftCounter(Channel("picB_ch4"))
