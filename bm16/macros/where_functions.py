

### /!\ DEPRECATED /!\
### where (= print) functions should go directly into the specific macros, not here !













#from bliss.setup_globals import dsp7230, regul_mono
#from bliss.shell.standard import wm
#from bliss.common.standard import iter_axes_position, iter_axes_position_all
#from tabulate import tabulate

# def wslits(**kwargs):
# """
# Displays slits positions in both user and dial units
# """
# print('################## Entrance Slits ##################')
# wm('vg1','vo1','hg1','ho1')
# print('\n########### Slits before and after Mono ############')
# wm('vg2','vo2','hg3','ho3')
# print('\nEncoder for su2:    '+f'{su2enc.read()}')
# print('Encoder for sd2:    '+f'{sd2enc.read()}')

# def wsample(**kwargs):
# """
# Displays sample stage positions in both user and dial units
# """
# print('########################## Sample stage and Detector ########################')
# wm('he','tte','re','be1','be2','ttd')

# def wmirrors(**kwargs):
# """
# Displays mirrors stage positions in both user and dial units
# """
# print('########################## M1 ########################')
# wm('mh1','mt1')
##wm('mh1','ma1','mc1','mt1')
# print('Encoder:    '+f'{mh1enc.read()}')
# print('\n########################## M2 ########################')
# wm('mh2')
##wm('mh2','ma2','mc2','mt2')
# print('Encoder:    '+f'{mh2enc.read()}')


''' De chez BM32
def w30(*axis_to_print, **kwargs):
    """
    Displays motors positions in a custumized format
    """

    max_cols = kwargs.get("max_cols", 9)
    header, pos, dial = [], [], []
    tables = [(header, pos, dial)]

    data = iter_axes_position(*axis_to_print, **kwargs)

    for axis in data:
        if len(header) == max_cols:
            header, pos, dial = [], [], []
            tables.append((header, pos, dial))

        axis_label = axis.axis_name
        if axis.unit:
            axis_label += f"[{axis.unit}]"

        header.append(axis_label)
        pos.append(axis.user_position)
        dial.append(axis.dial_position)

    for table in tables:
        print("")
        print(tabulate(table, headers="firstrow", floatfmt=".05f", numalign="right"))
'''
