import sys
import logging
from datetime import datetime
from bliss.common.logtools import get_logger
from bliss.setup_globals import euromono, s7mono, BLmonitorMG
from bliss.common.scans.step_by_step import loopscan
from bm16.scans.channels_export import CounterChannelScanPreset

p = CounterChannelScanPreset(*euromono.counters, *s7mono.counters)


def get_bl_status_logger():
    sys.stdout.flush()
    _logger = get_logger("bm16.status")
    _logger.setLevel("INFO")
    _logger._fname = f"/data/bm16/inhouse/Monitoring_Temp_Vide/status/status_{datetime.today().strftime('%Y-%m-%d')}.log"
    _logger._fmt = "%(asctime)s %(levelname)-s : %(message)s"

    _file_handler = logging.FileHandler(_logger._fname, mode="a")
    _file_handler.setFormatter(logging.Formatter(fmt=_logger._fmt))
    _logger.addHandler(_file_handler)

    _console_handler = logging.StreamHandler(sys.stdout)
    _console_handler.setFormatter(logging.Formatter(fmt=_logger._fmt))
    _logger.addHandler(_console_handler)
    return _logger


def bl_monitor(nb_loops):
    for i in range(nb_loops):
        s = loopscan(60, 1, BLmonitorMG, run=False)
        s.add_preset(p)
        s.run()
        i += 1


def bl_permanent_monitor():
    _logger = get_bl_status_logger()
    _logger.info("started permanent monitor loop")
    _logger.info(f"=> {_logger._fname}")
    while True:
        try:
            from bliss.scanning.scan_info import ScanInfo
            scan_info = ScanInfo()
            scan_info.add_curve_plot(x="timer:epoch")
            s = loopscan(360, 1, BLmonitorMG, sleep_time=9, run=False, scan_info=scan_info)
            s.add_preset(p)
            s.run()
        except Exception as e:
            _logger.error(e)
            pass


