from bliss.config.channels import Channel
from bliss.setup_globals import picB
import bliss.common.utils as utils
from bliss.common.scans import DEFAULT_CHAIN
from bliss.scanning.chain import ChainPreset, ChainIterationPreset


class PausePicoMonPreset(ChainPreset):
    """
    This preset will pause the monitoring loop during a scan
    """

    def __init__(self):
        self.monitoring_active = Channel("pico_monitoring", True)

    def prepare(self, chain):
        self.monitoring_active.value = False

    def stop(self, chain):
        self.monitoring_active.value = True


def pico_mon():
    monitoring_active = Channel("pico_monitoring")
    if monitoring_active.value:
        picB.ntoread = 4
        picB._ioRW("NAQ 1")
        picB.start()
        picB_readall = picB.read_all(*picB.counters)


def pico_mon_init():
    preset = PausePicoMonPreset()
    DEFAULT_CHAIN.add_preset(preset)


pico_monitor = utils.periodic_exec(1, pico_mon)
