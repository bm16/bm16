from bliss.setup_globals import *
from bm16.macros.bl_distances import add_params_to_globals
import math

add_params_to_globals()


def moveM1M2angle(new_angle, shift_m1=0, shift_m2=0, do_move=False):
    """ Calculate motors positions for a given angle of the mirrors
    
    Parameters
    -----------
    
    new_angle : angle of the mirros in mrad
    shift_m1
    shift_m2
    do_move : move the motors [False]
    
    """
    old_angle = ma1.position

    outstr = (
        f"Move mirrors angles (ma1, ma2): {old_angle:.1f} -> {new_angle:.1f} mrad\n"
    )

    # Calculate new positions
    new_vg1 = new_angle * P_M1.length * P_SLITSV1.dist / P_M1.dist
    outstr += (
        f"vg1: {vg1.position:.3f} -> {new_vg1:.3f} ({new_vg1 - vg1.position:.3f})\n"
    )

    new_mh1 = mh1.position + shift_m1
    outstr += (
        f"mh1: {mh1.position:.3f} -> {new_mh1:.3f} ({new_mh1 - mh1.position:.3f})\n"
    )

    relmove_vo2 = 2 * (new_angle - old_angle) * (P_SLITSV2.dist - P_M1.dist)
    new_vo2 = vo2.position + relmove_vo2
    outstr += f"vo2: {vo2.position:.3f} -> {new_vo2:.3f} ({relmove_vo2:.3f})\n"

    new_vg2 = new_angle * P_M2.length
    outstr += (
        f"vg2: {vg2.position:.3f} -> {new_vg2:.3f} ({new_vg2 - vg2.position:.3f})\n"
    )

    relmove_hm = 2 * (new_angle - old_angle) * (P_MONO.dist - P_M1.dist)
    new_hm = hm.position + relmove_hm
    outstr += f"hm: {hm.position:.3f} -> {new_hm:.3f} ({relmove_hm:.3f})\n"

    relmove_mh2 = shift_m2 + 2 * (new_angle - old_angle) * (P_M2.dist - P_M1.dist)
    new_mh2 = mh2.position + relmove_mh2
    outstr += f"mh2: {mh2.position:.3f} -> {new_mh2:.3f} ({relmove_mh2:.3f})\n"

    relmove_vo4 = 2 * (new_angle - old_angle) * (P_M2.dist - P_M1.dist)
    new_vo4 = vo4.position + relmove_vo4
    outstr += f"vo4: {vo4.position:.3f} -> {new_vo4:.3f} ({relmove_vo4:.3f})\n"

    relmove_tz = 2 * (new_angle - old_angle) * (P_M2.dist - P_M1.dist)
    new_tz = tz.position + relmove_tz
    outstr += f"tz: {tz.position:.3f} -> {new_tz:.3f} ({relmove_tz:.3f})\n"

    old_bragg = braggenc.read()
    new_bragg = old_bragg - 2 * (new_angle - old_angle) / 1000 * 180 / math.pi
    outstr += f"bragg: {old_bragg:.4f} -> {new_bragg:.4f}\n"

    print(outstr)

    if do_move:
        print("Closing the Front-End...")
        frontend.close()
        print("Moving the motors:")
        umv(vg1, new_vg1)
        umvr(mh1, shift_m1)
        umv(ma1, new_angle)
        umvr(vo2, relmove_vo2)
        umv(vg2, new_vg2)
        umvr(hm, relmove_hm)
        umvr(mh2, relmove_mh2)
        umv(ma2, new_angle)
        umvr(vo4, relmove_vo4)
        umvr(tz, relmove_tz)
        print(
            "Setting the bragg angle to its new value (given by the new mirror angle)"
        )
        braggenc.set(new_bragg)
        print("Opening the Front-End...")
        frontend.open()
