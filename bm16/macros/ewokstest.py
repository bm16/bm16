from ewoksjob.client import submit
from bliss import current_session
from bliss.common.plot import get_flint


def mypic(x="epoch", y="I1"):
    inputs=[{"id":"node1", "name": "filename", "value":current_session.scan_saving.filename},
            {"id":"node1", "name": "x", "value":x},
            {"id":"node1", "name": "y", "value":y},]

    result = submit(args=("picgraph.json",), kwargs={"inputs": inputs}).get()
    x,y = result["pic"]
    myplot(x,y)


def myplot(x,y):
    f = get_flint()
    p = f.get_plot("curve", name="My plot")
    p.add_curve_item("x", "y")
    p.set_data(x=x, y=y)
