from bliss.config.static import get_config


def tz2icepap():
    """move tz via Icepap axis"""
    tz_icepap = get_config().get("tz_icepap")
    tz = get_config().get("tz")
    tz_icepap.dial = tz.position
    tz_icepap.offset = 0
    tz_icepap.hw_state
    tz_icepap.controller.raw_write(f"{tz_icepap.address}: INFOA HIGH")


def icepap2tz():
    """move tz via hexapode controller"""
    tz_icepap = get_config().get("tz_icepap")
    tz = get_config().get("tz")
    tz_icepap.hw_state
    tz_icepap.controller.raw_write(f"{tz_icepap.address}: INFOA LOW")
    tz.sync_hard()
