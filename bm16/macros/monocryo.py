#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
BM16 monochromator cryo cooler
==============================
"""
import sys
import time
from datetime import datetime
import logging

from bliss.config.static import get_config
from bliss.common.logtools import get_logger
from bliss.config.channels import Channel

REG_SELFA = 51800
REG_REFILL = 4964

sys.stdout.flush()

_logger = get_logger("bm16.monocryo")
_logger.setLevel("INFO")
_logger._fname = f"/data/bm16/inhouse/Monitoring_Temp_Vide/monocryo/monocryo_{datetime.today().strftime('%Y-%m-%d')}.log"
_logger._fmt = "%(asctime)s %(levelname)-s : %(message)s"

_file_handler = logging.FileHandler(_logger._fname, mode="a")
_file_handler.setFormatter(logging.Formatter(fmt=_logger._fmt))
_logger.addHandler(_file_handler)

_console_handler = logging.StreamHandler(sys.stdout)
_console_handler.setFormatter(logging.Formatter(fmt=_logger._fmt))
_logger.addHandler(_console_handler)

config = get_config()
euromono = config.get("euromono")
selfa = Channel("euromono:Selfa")
v2 = Channel("s7mono:V2")  #: V2 status
ln2 = Channel("s7mono:ln2")  #: % of LN2
pln2 = Channel("s7mono:pln2")  #: pressure of LN2
# s7mono = config.get("s7mono")


def monocryo_info():
    """Get info on the monocromator LN2"""
    try:
        out = ["MONOCHROMATOR LN2 INFO"]
        out.append("----------------------")
        out.append(f"Level: {ln2.value:.2f}%")
        out.append(f"Pressure: {pln2.value:.2f} bar")
        out.append(f"V2 is {get_v2_status()}")
        out.append(f"selfa at {selfa_status(verbose=False)}%")
        print("\n".join(out))
    except Exception:
        _logger.error("Channel failed getting values [HINT: just repeat this command]")


def selfa_status(verbose=True):
    """get selfa status"""
    try:
        _selfa = euromono.read_all(euromono.counters[2])[0]
    except Exception:
        _selfa = selfa.value
    if _selfa is None:
        _logger.error("SELFA STATUS UNKNOWN!!!")
    _msg = f"selfa open at {_selfa} %"
    if verbose:
        _logger.info(_msg)
    else:
        _logger.debug(_msg)
    return _selfa


def selfa_open(val):
    """open selfa to a given value"""
    euromono.com.write_float(REG_SELFA, val)
    selfa_status(verbose=True)


def refill_mono_start():
    """Starts the LN2 refill procedure"""
    # 1) start the refill procedure at the Siemens S7
    euromono.com.write_register(REG_REFILL, "H", 1)
    _logger.info("START MONO LN2 REFILL PROCEDURE")
    time.sleep(1)
    euromono.com.write_register(REG_REFILL, "H", 0)


def get_v2_status():
    v2status = ["open", "closed"]
    return v2status[int(v2.value)] or "unknown"


def refill_mono_procedure(ln2_threshold=None):
    """Performs the LN2 refill procedure once"""
    if (ln2_threshold is not None) and (check_mono_ln2_level(ln2_threshold) is False):
        _logger.debug("monochromato LN2 refill procedure skipped")
        return
    refill_mono_start()
    v2_open_state = False
    cooling_line = False
    refill_done = False
    selfa_before = selfa_status(verbose=False)
    sleep_wait = 10
    try:
        while not refill_done:
            _v2 = get_v2_status()
            assert _v2 in ["open", "closed", "unknown"], f"wrong V2 status [{_v2}]"
            _logger.debug(
                f"V2: {_v2}, selfa: {selfa_status(verbose=False)}, v2_open_state: {v2_open_state}, refill_done: {refill_done}"
            )
            if (_v2 == 'closed') and (v2_open_state is False) and (cooling_line is False):
                _logger.info("cooling LN2 line... (it may take more than 20 minutes)")
                cooling_line = True
            if (_v2 == 'open') and (v2_open_state is False):  # V2 open
                _logger.info("refilling LN2 tank... (it may take more than 30 minutes)")
                selfa_open(100)
                v2_open_state = True
            elif (_v2 == 'closed') and (v2_open_state is True):
                selfa_open(selfa_before)
                v2_open_state = False
                refill_done = True
                _logger.info("monochromator LN2 refill: DONE")
                break
            _logger.debug(f"waiting {sleep_wait} s before next check...")
            time.sleep(sleep_wait)
    except KeyboardInterrupt:
        _logger.warning(f"monochromator LN2 refill INTERRUPTED: selfa back to {selfa_before} %")
        selfa_open(selfa_before)
        return


def check_mono_ln2_level(pcthreshold):
    """Checks if the monocrhomator LN2 level is below a given percentange"""
    ln2_level = ln2.value
    while ln2_level is None:
        _logger.error("CANNOT READ LN2 LEVEL !!!")
        ln2_level = ln2.value
        time.sleep(1)
    _logger.info(f"LN2 level is {ln2_level:.2f}%")
    if ln2_level <= pcthreshold:
        return True
    else:
        return False
