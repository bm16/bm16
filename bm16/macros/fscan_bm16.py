import click

from bliss import setup_globals
from bliss.scanning.scan import ScanPreset
from bliss.scanning.chain import ChainPreset

from bliss.shell.cli.user_dialog import UserYesNo
from bliss.shell.cli.pt_widgets import display

from fscan.fscantools import FScanMode

def dfscan(motor, start, stop, nb_points, expo_time, latency_time=0, save=True, verbose=False, confirm=True):
    """
    Start hardware triggered time scan on detectors with MUSST card as the master.
    To select the detectors, enable/disable them using the ACTIVE_MG measurement group.
    Args:
        motor: the motor to move
        start: the motor relative start position
        stop:  the motor relative stop postion
        nb_points: number of acquistion points
        exp_time: exposure time in seccan.chain_show()
        Scan: the scan handler
    """

    # todo calculate the minimum lima latency to set the step_time bigger than the acq_time
    camera_latency = dict()

    # adjust the step size according to start/stop/nbpoints
    step_size = (stop - start) / nb_points

    fscan = setup_globals.fscan
    

    motor_initial_pos = motor.position
    print("the motor current positino is: ", motor_initial_pos)

    #
    # set parameters for fscan
    #
    # Add a latency if requested, no longer change the period, ftimescan use the camera calibration
    fscan.pars.latency_time = latency_time

    fscan.pars.motor = motor
    fscan.pars.start_pos = motor_initial_pos + start
    fscan.pars.step_size = step_size
    fscan.pars.npoints = nb_points
    fscan.pars.acq_time = expo_time
    fscan.pars.step_time = expo_time + latency_time

    # always generate trig in time
    fscan.pars.scan_mode = FScanMode.TIME #time mode
    #fscan.pars.scan_mode = FScanMode.POSITION #position mode
    # always generate gate according to step_time and not to step_size
    fscan.pars.gate_mode = FScanMode.TIME #time mode
    #fscan.pars.gate_mode = FScanMode.POSITION #position mode
    
    # synchronize musst counter with real motor position
    fscan.pars.sync_encoder = True
    # do not deal with an acceleration margin, trust in ICEPAP
    fscan.pars.acc_margin = 0
    fscan.pars.save_flag = save
    
    if verbose:
        fscan.show()
        
    fscan.prepare()
    if verbose:
        fscan.chain_show()

    # ask bliss scan engine to move the motors to start positions at the end of the scan
    fscan.scan.restore_motor_positions = True
    
    if not confirm or (confirm and click.confirm('Do you want to continue?', default=True)):        
        fscan.start()
        return fscan.scan

    return None


def afscan(motor, start, stop, nb_points, expo_time, latency_time=0, save=True, verbose=False, confirm=True):
    """
    Start hardware triggered time scan on detectors with MUSST card as the master.
    To select the detectors, enable/disable them using the ACTIVE_MG measurement group.
    Args:
        motor: the motor to move
        start: the motor start position
        stop:  the motor stop position
        nb_points: number of acquistion points
        exp_time: exposure time in second
        latency_time:  time to wait between 2 points [default: 0]
        save: choose to save or not the data [default: True]
    Returns:
        Scan: the scan handler
    """

    # todo calculate the minimum lima latency to set the step_time bigger than the acq_time
    camera_latency = dict()

    # adjust the step size according to start/stop/nbpoints
    step_size = (stop - start) / nb_points

    fscan = setup_globals.fscan
    

  
    #
    # set parameters for fscan
    #
    # Add a latency if requested, no longer change the period, ftimescan use the camera calibration
    fscan.pars.latency_time = latency_time

    fscan.pars.motor = motor
    fscan.pars.start_pos = start
    fscan.pars.step_size = step_size
    fscan.pars.npoints = nb_points
    fscan.pars.acq_time = expo_time
    fscan.pars.step_time = expo_time + latency_time

    # always generate trig in time
    fscan.pars.scan_mode = FScanMode.TIME #time mode
    #fscan.pars.scan_mode = FScanMode.POSITION #position mode
    # always generate gate according to step_time and not to step_size
    fscan.pars.gate_mode = FScanMode.TIME #time mode
    #fscan.pars.gate_mode = FScanMode.POSITION #position mode
    
    # synchronize musst counter with real motor position
    fscan.pars.sync_encoder = True
    # do not deal with an acceleration margin, trust in ICEPAP
    fscan.pars.acc_margin = 0
    fscan.pars.save_flag = save
    
    if verbose:
        fscan.show()
        
    fscan.prepare()
    if verbose:
        fscan.chain_show()

    # ask bliss scan engine to move the motors to start positions at the end of the scan
    fscan.scan.restore_motor_positions = False

    if not confirm or (confirm and click.confirm('Do you want to continue?', default=True)):        
        fscan.start()
        return fscan.scan

    return None


class CountMuxPreset(ChainPreset):
    def __init__(self, mux):
        self.mux = mux
        self._acq_mode = 'COUNT'
        ChainPreset.__init__(self)
                
    @property
    def acq_mode(self):
        return self._acq_mode
    
    @acq_mode.setter
    def acq_mode(self, mode):
        assert isinstance(mode, str)
        assert (mode.upper() == 'COUNT' or mode.upper() == 'ACCUMULATION')
        self._acq_mode = mode
        
    def prepare(self, chain):
        self.mux.switch('ACQ_TRIG_MODE', self._acq_mode)
        

class FScanMuxPreset(ScanPreset):
    def __init__(self, mux):
        self.mux = mux
        ScanPreset.__init__(self)
                
    def prepare(self, scan):
        pass

    def start(self, scan):
        self.mux.switch('ACQ_TRIG_MODE', 'FSCAN')
        
    def stop(self, scan):
        self.mux.switch('ACQ_TRIG_MODE', 'COUNT')
