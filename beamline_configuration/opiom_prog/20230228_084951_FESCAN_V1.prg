// INPUT
wire MUSST_ATRIG;
wire MUSST_BTRIG;
wire P201_GATE;
wire IN_TRIG1;
wire IN_TRIG2;

assign MUSST_ATRIG = I1;
assign MUSST_BTRIG = I2;
assign P201_GATE = I3;
assign IN_TRIG1 = I4;
assign IN_TRIG2 = I5;

// INCREASED SIGNAL LENGTH
// 12 bit = pow(2,12)/16000000 = 256us
wire inp_1;
wire MUSST_ATRIG_LONG;

reg [12:0] CNT_120;
reg inp_aux1;
reg inp_aux2;
reg cnt_start;

assign inp_1 = MUSST_ATRIG;
assign MUSST_ATRIG_LONG = CNT_120[12];

// SELECTION
wire [1:0]SEL_TRIG;
wire [1:0]SEL_ITRIG_SRC;

assign SEL_TRIG[1:0] = {IMA2,IMA1};
assign SEL_ITRIG_SRC[1:0] = {IMA4, IMA3};

reg ITRIG_SRC;

//OUTPUT
reg MUSST_ITRIG_OUT;
reg P201_OUT;
reg AH_OUT;
reg XPAD_OUT;
reg KETEK_OUT;
reg SHUT_OUT;

assign O1 = MUSST_ITRIG_OUT;
assign O2 = P201_OUT;
assign O3 = AH_OUT;
assign O4 = XPAD_OUT;
assign O5 = KETEK_OUT;
assign O6 = SHUT_OUT;

always @(SEL_ITRIG_SRC or IN_TRIG1 or IN_TRIG2)
begin
   case (SEL_ITRIG_SRC)
      2'b01: ITRIG_SRC = 1'b1;
      2'b10: ITRIG_SRC = IN_TRIG1;
      2'b11: ITRIG_SRC = IN_TRIG2;
      default : ITRIG_SRC = 1'b0;
   endcase
end

always @(SEL_TRIG or ITRIG_SRC or MUSST_BTRIG or MUSST_ATRIG or P201_GATE or MUSST_ATRIG_LONG)
begin
   case (SEL_TRIG)
      2'b01: begin
              MUSST_ITRIG_OUT = ITRIG_SRC;
              P201_OUT = MUSST_BTRIG;
              AH_OUT = MUSST_BTRIG;
              XPAD_OUT = MUSST_BTRIG;
              KETEK_OUT = MUSST_BTRIG;
              SHUT_OUT = 0;
            end
      2'b10: begin
              MUSST_ITRIG_OUT = ITRIG_SRC;
              P201_OUT = MUSST_ATRIG;
              AH_OUT = ~MUSST_ATRIG_LONG;
              XPAD_OUT = MUSST_ATRIG;
              KETEK_OUT = MUSST_ATRIG;
              SHUT_OUT = MUSST_BTRIG;
            end
      2'b11: begin
              MUSST_ITRIG_OUT = P201_GATE;
              P201_OUT = 0;
              AH_OUT = ~P201_GATE;
              XPAD_OUT = MUSST_ATRIG;
              KETEK_OUT = P201_GATE;
              SHUT_OUT = P201_GATE;
             
            end
      default: begin      
              MUSST_ITRIG_OUT = ITRIG_SRC;
              P201_OUT = 0;
              AH_OUT = P201_GATE;
              XPAD_OUT = ~P201_GATE;
              KETEK_OUT = P201_GATE;
              SHUT_OUT = P201_GATE;
            end
    endcase
end

// Generate Increased MUSST_ATRIG length signal
always @(posedge CLK16)
begin
  inp_aux1 <= inp_1;
  inp_aux2 <= inp_aux1;
end

always @(posedge CLK16)
  begin
    if (inp_aux1 && ~inp_aux2) 
       cnt_start <= 1;
    else
       cnt_start <= 0;
  end

always @(posedge CLK16)
  begin
    if (cnt_start)
      CNT_120 <= 0'h0000;
    else if (CNT_120[12])
      CNT_120 <= CNT_120;
    else
      CNT_120 <= CNT_120 + 1;
  end

// Rear panel outputs
assign {OB8, OB7, OB6, OB5, OB4, OB3, OB2, OB1} = I; 

// Internal outputs towards microcontroller
assign {OM8, OM7, OM6, OM5, OM4, OM3, OM2, OM1} = ~0;

// Clock outputs towards microcontroller
assign {CLKD, CLKC, CLKB, CLKA} = I[4:1]; 


