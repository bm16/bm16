The opiom program code is saved on https://opiom.esrf.fr/ under "Others" project directory as "BCULab opiom1" .

G. Berruyer 2023-02-22

I1 <-- TRIG OUTA MUSST
I2 <-- TRIG OUTB MUSST
I3 <-- GATE P201 channel 10
I4 <-- 
I5 <-- 
I6 <--
I7 <--
I8 <-- 

O1 --> TRIG IN MUSST (TTL)
O2 --> TRIG P201 channel 9
O3 --> TRIG A401D
O4 --> GATE XPAD (TODO)
O5 --> GATE XIA MERCURY (TODO)
O6 --> TRIGA MUSST
O7 --> GATE P201 (fast shutter)
O8 --> TRIGA MUSST LONG (250 us)

