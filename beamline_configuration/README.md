# Beamline configuration for BM16

This directory contains the beamline configuration files (motors, counters, pseudos,
sessions, etc.) used by Beacon.

It is separated from the :mod:`bm16` library for two reasons:

- It may be modified directly via the Beacon web interface (port 9030).

- Multiple copies of this directory may exist for launching the Beacon server
  with various configurations. A typical example is to work with mockup motors
  during development without changing motors names.

  This can be done in a development branch, launching a separate beacon-sever
  instance::

    beacon-server --db_path=~/local/bmxx/beamline_configuration/ --port=26000 --redis_port=6479 --webapp-port=9130
    BEACON_HOST=localhost:26000; bliss -s test
