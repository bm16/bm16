from bliss import current_session
from bm16.macros.monocryo import *

current_session.disable_esrf_data_policy()
current_session.scan_display.auto = True  # display scans

_ss = current_session.scan_saving

_ss.base_path = "/data/bm16/inhouse/Monitoring_Temp_Vide"
_ss.date_format = "%Y-%m-%d"
_ss.template = "{session}"
_ss.data_filename = "monitor_{date}"

print(f"INFO: saving data to {_ss.get_path()}")
