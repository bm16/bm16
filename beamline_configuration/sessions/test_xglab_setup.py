
from bliss.setup_globals import *
from bliss import current_session

load_script("test_xglab.py")

current_session.disable_esrf_data_policy()
current_session.scan_saving.base_path = "/data/bm16/inhouse/bliss"
current_session.scan_saving.date_format = "%Y-%m-%d"
current_session.scan_saving.template = "{session}"
current_session.scan_saving.data_filename = "{session}_{date}"
current_session.writer = "nexus"

print("Setting scanfile to", current_session.scan_saving.get_path())

# workaround a bliss bug
SCAN_SAVING = current_session.scan_saving
