
from bliss.setup_globals import *

load_script("eurotest.py")

print("")
print("Welcome to your new 'eurotest' BLISS session !! ")
print("")
print("You can now customize your 'eurotest' session by changing files:")
print("   * /eurotest_setup.py ")
print("   * /eurotest.yml ")
print("   * /scripts/eurotest.py ")
print("")

from bm16.macros.bl import *
bl = BL()
from bm16.macros.blstatus_channels_import import *

