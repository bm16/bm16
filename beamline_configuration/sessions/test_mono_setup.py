#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Setup for tests with the monochromator
=======================================
"""

from bliss.setup_globals import *
from bliss import current_session
from bm16.macros.bl_utils import shcl, shop
from bm16.macros.shexapod_icepap import *

#load_script("mono_test.py")

current_session.disable_esrf_data_policy()
current_session.scan_saving.base_path = "/data/bm16/inhouse/bliss"
current_session.scan_saving.date_format = "%Y-%m-%d"
current_session.scan_saving.template = "{session}"
current_session.scan_saving.data_filename = "mono_{date}"
current_session.writer = "nexus"

print("Setting scanfile to", current_session.scan_saving.get_path())
