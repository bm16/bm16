from bliss import current_session
from bm16.macros.bl import BL
from bm16.macros.blstatus_monitor import *
from bm16.macros.monocryo import *

#configure current session
current_session.disable_esrf_data_policy()
current_session.scan_saving.writer="nexus"
current_session.scan_saving.base_path = "/data/bm16/inhouse/Monitoring_Temp_Vide"
current_session.scan_saving.date_format = "%Y-%m-%d"
current_session.scan_saving.template = "{session}"
current_session.scan_saving.data_filename = "monitor_{date}"

current_session.scan_display.auto = True  # display scans
current_session.scan_display.displayed_channels = ["euromono:T_euro"]
#current_session.scan_display.flint_output_enabled = True

print("Setting scanfile to", current_session.scan_saving.get_path())

bl = BL()
BLmonitorMG.set_active()
