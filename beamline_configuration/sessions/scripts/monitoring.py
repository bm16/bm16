from bliss.scanning.monitoring import start_monitoring, stop_monitoring, stop_all_monitoring

start_monitoring("picomonitor", 0.2, picB, sleep_time=0.5)

print("Started pico monitoring -> select I0 counter in the plot if you do not see it")
