# Scripts for sessions setups

These scripts are run via the `load_script("myscript.py")` function called in the session setup files.

They are simply import calls to `bm16.macros.myscript` modules.