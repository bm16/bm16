
from bliss.scanning.scan import ScanPreset
from bliss.scanning.chain import ChainPreset

class FScanTestPreset(ScanPreset):
    def __init__(self, mux):
        self.mux = mux

    def prepare(self, scan):
        self.mux.switch("ACQ_TRIG_MODE", "FSCAN")

class CountTestPreset(ChainPreset):
    def __init__(self, mux):
        self.mux = mux

    def prepare(self, chain):
        self.mux.switch("ACQ_TRIG_MODE", "COUNT")
