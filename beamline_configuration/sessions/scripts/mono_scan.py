from bm16.macros.shexapod_icepap import tz2icepap, icepap2tz

#if 1:
#	tz2icepap()
#	print("INFO: tz_icepap enabled")
#else:
#	icepap2tz()
#	print("INFO: tz_icepap disabled (=> tz mono tracking off)")
