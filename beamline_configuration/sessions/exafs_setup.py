#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Setup file for the main beamline session (aka exafs)
====================================================

..note:: The main beamline session contains all the components of the beamline
         and may be difficult to debug, this is why most of the configuration
         and macros is loaded modularly via the `load_script` calls, which are
         mainly imports from `bm16.macros` modules -> ask Mauro if this is not
         clear!

"""

from bliss.setup_globals import *
from bliss import current_session

print("")
print("Welcome to 'exafs' BLISS session!! ")
print("")

#: the following scripts are helper to easily load/unload components of the beamline (speedin up testing)

load_script("bl_utils.py")  #: various beamline utils
load_script("mono_scan.py")  #: monochromator scanning tools
#load_script("mono_piezo.py")  #: monochromator feedback /// TODO
#load_script("monitoring.py")  #: pico monitoring //// NOT WORKING YET!!!

if 0:
    """to use for testing/commissioning purposes"""
    current_session.disable_esrf_data_policy()
    current_session.scan_saving.base_path = "/data/bm16/inhouse/bliss"
    current_session.scan_saving.date_format = "%Y-%m-%d"
    current_session.scan_saving.template = "{session}"
    current_session.scan_saving.data_filename = "exafs_{date}"
    current_session.writer = "nexus"
else:
    current_session.enable_esrf_data_policy()


print("Setting scanfile to", current_session.scan_saving.get_path())

mg_exafs.set_active()
SCAN_DISPLAY.auto=True


"""
# Set as default chain, hardware trigger for 2D detectors by p201
DEFAULT_CHAIN.set_settings(chain_exafs_hardware_trig["chain_config"])


#######  setup for fast scan  ######
from bm16.macros.fscan_bm16 import afscan, dfscan,  CountMuxPreset, FScanMuxPreset

count_mux = CountMuxPreset(mux1)
DEFAULT_CHAIN.add_preset(count_mux)

# fast scans objects, be used by hooscan() help and can be used to re-run with dry and scope
fscan_mux  = FScanMuxPreset(mux1)
fscan_config.add_scan_preset(fscan_mux)
# preset can be added just on one fscan, above we apply to all (fscan,timescan,fscan2d ..)
fscan   = fscan_config.fscan
fscan2d = fscan_config.fscan2d
"""
