import os
#import math
#import numpy as np
#from bliss.common.standard import *
#from bliss.setup_globals import *

from bliss import current_session

if 0:
    #used during tests/commissioning
    current_session.disable_esrf_data_policy()
    current_session.scan_display.auto = False  # display scans
    current_session.scan_saving.base_path = "/data/bm16/inhouse/bliss"
    current_session.scan_saving.template = "{session}"
    current_session.scan_saving.data_filename = "mauro_{date}"
else:
    #enable data policy by default
    current_session.enable_esrf_data_policy()

print("Setting scanfile to", current_session.scan_saving.get_path())#
