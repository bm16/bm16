#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Setup for BM16 mini-cryostat "Ricardo"
======================================
"""
from bliss import current_session
#from bliss.setup_globals import SCAN_SAVING, ls336cryo
from bliss.common.scans.step_by_step import loopscan
from bm16.scans.channels_export import CounterChannelScanPreset


def cryo_permanent_monitor():
    #_sp = CounterChannelScanPreset(*ls336cryo.counters)
    while True:
        try:
            s = loopscan(360, 1, sleep_time=9, run=False)
            #s.add_preset(_sp)
            s.run()
        except Exception as e:
            print(e)
            pass

#
current_session.disable_esrf_data_policy()
current_session.scan_saving.writer = "nexus"
current_session.scan_saving.base_path = "/data/bm16/inhouse/Monitoring_Temp_Cryo"
current_session.scan_saving.date_format = "%Y-%m-%d"
current_session.scan_saving.template = "{session}"
current_session.scan_saving.data_filename = "cryo_{date}"
#
current_session.scan_display.auto = True  # display scans
current_session.scan_display.displayed_channels = ['Tsample_K:T_sample_K_counter']

print(f"INFO: saving data to {current_session.scan_saving.get_path()}")
