#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Setup file for the `spectro14` test session
===========================================
"""

#import os
#import math
#from bliss.common.standard import *
#from bliss.setup_globals import *
#
#print('START spectro14_setup.py')
#
#try:
#    from bm16.macros.spectro14_align import *
#    print('INFO: all spectro14 macros imported')
#except ImportError:
#    print('WARNING: no spectro14 macros imported')

from bliss import current_session
#current_session.enable_esrf_data_policy()
current_session.disable_esrf_data_policy()
current_session.scan_saving.base_path = "/data/bm16/inhouse/bliss"
current_session.scan_saving.date_format = "%Y-%m-%d"
current_session.scan_saving.template = "{session}"
current_session.scan_saving.data_filename = "spectro_{date}"
current_session.writer = "nexus"

print("Setting scanfile to", current_session.scan_saving.get_path())

print("INFO: SPECTROMETER OBJECT IS 's'")