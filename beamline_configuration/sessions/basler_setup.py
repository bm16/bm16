import os
from bliss.setup_globals import SCAN_SAVING
from bliss import current_session

current_session.disable_esrf_data_policy()

current_session.scan_display.auto = True  # display scans
current_session.scan_saving.base_path = "/data/bm16/inhouse/bliss"
current_session.scan_saving.template = "{session}"
current_session.scan_saving.data_filename = "bm16basler_{date}"
print("Setting scanfile to", current_session.scan_saving.get_path())
