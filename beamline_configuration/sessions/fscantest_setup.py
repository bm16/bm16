
from bliss.setup_globals import *

load_script("fscantest.py")

#SCAN_DISPLAY.auto = True
SCAN_DISPLAY.auto = False


# --- default chain preset
_count_preset = CountTestPreset(mux1)
DEFAULT_CHAIN.add_preset(_count_preset)

# --- fscan config
_fscan_config = config.get("fscan_config")
_fscan_preset = FScanTestPreset(mux1)
_fscan_config.add_scan_preset(_fscan_preset)

ftimescan = _fscan_config.ftimescan
ftimescanlookup = _fscan_config.ftimescanlookup
fscan = _fscan_config.fscan
fscan2d = _fscan_config.fscan2d
fscan3d = _fscan_config.fscan3d
f2scan = _fscan_config.f2scan
fsweep = _fscan_config.fsweep
finterlaced = _fscan_config.finterlaced
# fscanloop = _fscan_config.fscanloop

# # --- fscanloop trigger
# def pulse():
#      mux1.switch("MUSST_ITRIG_SRC", "SETON")
#      mux1.switch("MUSST_ITRIG_SRC", "SETOFF")

# fscanloop.set_lima_ready_trigger_func(pulse)


