import os
from bliss.setup_globals import *
from bliss.common.standard import loopscan
from bliss.common.counter import SoftCounter
from bm16.scans.PELscans import *
from bliss import current_session

current_session.scan_display.auto = False  # display scans
current_session.scan_saving.base_path = "/data/bm16/inhouse/bliss"
current_session.scan_saving.template = "{session}"
current_session.scan_saving.data_filename = "bliss_{date}"
print("Setting scanfile to", current_session.scan_saving.get_path())