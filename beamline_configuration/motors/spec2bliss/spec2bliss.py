#python3 ./spec2bliss.py config

with open('config', 'r') as config_file:
    config = config_file.read()

    #Get List Off Nodes
    headerNodes="# Device nodes"
    nodes=list(filter(None,config.split(headerNodes)[1].split("# ")[0].split("\n")))


    # Motor    cntrl steps sign slew base backl accel nada flags   mne  name
    headerMotors="# Motor    cntrl steps sign slew base backl accel nada  flags   mne  name"
    motors=list(filter(None,config.split(headerMotors)[1].split("# ")[0].split("\n")))


#extract icepap from nodes
icepapIP=[]
icepapID=[]
ID=-1
for idx,node in enumerate(nodes):
    infoNode=node.split()
    if (infoNode[0]=="PSE_MAC_MOT"):
        ID+=1
    if (infoNode[2]=="icepap"):
        icepapIP.append(infoNode[4])
        icepapID.append(ID)


#extract motors with corresponding to icepap
#init icepapMOTOR 2d list -> icepapMOTOR[icepap_node][icepap_motor]
icepapMOTOR=[None]*len(icepapID)
for i in range(len(icepapMOTOR)):
    icepapMOTOR[i]=[]
#fill the list
for motor in motors:
    idNode=-1
    try:
        idNode=int(motor.split("MAC_MOT:")[1].split("/")[0])
        idx = icepapID.index(idNode)
        icepapMOTOR[idx].append(motor)
    except:
        pass

#convert icepap motors to bliss motor
for idx,ip in enumerate(icepapIP):
    print("-")
    print("  controller:")
    print("    class: icepap")
    print("    host: %s"%ip)
    print("    axes:")
    for motor in icepapMOTOR[idx]:
        motorInfo = motor.split()
        motorNodeInfo = motorInfo[2].split(":")[1].split("/")
        icepapCrate = int(motorNodeInfo[1])
        icepapCard = int(motorNodeInfo[2])
        steps_per_unit = float(motorInfo[3])
        sign = int(motorInfo[4])
        accelTime = int(motorInfo[8])
        speed = int(motorInfo[5])
        backlash = int(motorInfo[7])
        mne = motorInfo[11]
        print("      -")
        print("        name: %s"%mne)
        print("        address: %d"%(icepapCrate*10+icepapCard))
        print("        sign: %d"%(sign))
        print("        steps_per_unit: %f"%steps_per_unit)
        print("        velocity: %f"%(speed/(float(steps_per_unit))))
        print("        acceleration: %f"%((speed/(float(steps_per_unit))/(float(accelTime)/1000.))))
        print("        backlash: %f"%(backlash/(float(steps_per_unit))))
        print("        low_limit: -inf")
        print("        high_limit: inf")
        print("        offset: 0") 
