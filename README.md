# BLISS at BM16/FAME-UHD beamline

BLISS repository for BM16/FAME-UHD, French CRG beamline at ESRF.

FAME-UHD is dedicated to high-energy resolution x-ray absorption and emission
spectroscopy.

- [FAME-UHD homepage](http://www.esrf.fr/cms/live/live/en/sites/www/home/UsersAndScience/Experiments/CRG/BM16.html)
- [FAME-UHD wiki](http://wikiserv.esrf.fr/bm16/index.php/Main_Page)

## Documentation

This is a _WORK-IN-PROGRESS_ project, so there is not yet a real documentation.
Nevertheless, notes are kept during the development stage and a documentation
project (based on [Sphinx](https://www.sphinx-doc.org/en/master/index.html)) is
in the `docs/source` directory.

> Complementary documentation is also found at [BM30/FAME
> gitlab](https://gitlab.esrf.fr/F-CRG/bm30). The two beamlines have identical
> layout, apart the detection system, which is based on a crystal analyzer
> spectrometer on BM16.

For a list of changes to this project, refer to the [CHANGELOG](./CHANGELOG.md).

### Quick links from current docs

Here some quick links that may result useful at the current stage:

- [Notes on the monochromator](./docs/source/install_notes/monochromator.md).
- [Notes on acquisition chains](./docs/source/install_notes/acquisition_chains.md).
- [Notes on Supervisor](./docs/source/install_notes/supervisor.md).
  - Supervisor web interface at port **9001**.
  - Multivisor on port **22000**.
- [Mauro's quick notes on Git](https://xraysloth.readthedocs.io/en/latest/workflows/git.html).
