# Daemons (= services) controlled via Supervisor

BLISS services/daemons/processes on Linux machines at ESRF are controlled with
[Supervisor](http://supervisord.org). Here there are some quick notes of usage and
configuration.

## Quick tips

- Configuration files are located in `~blissadm/local/daemon/config/supervisor.d`
- The command line interface is `supervisorctl`
  - `reload` command permits reloading the configuration files.
- The web interface is accessible at `host:9001`.
