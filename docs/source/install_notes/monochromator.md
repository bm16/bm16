# Notes on BM16 monochromator

## Requirements

In a working BLISS environment (`bliss_dev` here), the following Python packaged must be installed:

- [pytest](https://docs.pytest.org/en/stable/)

    `conda install pytest`

- [xcalibu](https://github.com/cguilloud/xcalibu)

    `pip install xcalibu`

- [monochromator](https://gitlab.esrf.fr/bliss/monochromator) installed from source in `bliss_dev`:

    ```bash
    git clone https://gitlab.esrf.fr/bliss/monochromator
    cd monochromator
    pip install -e . --no-deps
    ```

- [bm16](https://gitlab.esrf.fr/F-CRG/bm16)

    ```bash
    git clone https://gitlab.esrf.fr/F-CRG/bm16
    cd bm16
    pip install -e . --no-deps
    ```

## Example of basic monochromator

An example showing the configuration (YML) required to run a simple Bragg/energy conversion in a monochromator is the following:

```yml
# TEST Monochromator
- plugin: bliss
  package: monochromator.monochromator
  class: MonochromatorBase
  name: bm16mono
  xtals: $bm16_mono_xtals
  energy_motor: $ene
  bragg_motor: $sim_mono

# Xtals Manager
- plugin: bliss
  package: monochromator.monochromator
  class: XtalManager
  name: bm16_mono_xtals
  xtals:
    - xtal: Si220
      dspacing: 1.920171445282458

# Energy Calc Motor  
- plugin: emotion
  package: monochromator.monochromator_calcmotor
  class: EnergyCalcMotor
  axes:
    - name: $sim_mono
      tags: real bragg
    - name: ene
      tags: energy
      unit: keV

# Simulated Real Monochromator Motors
- plugin: emotion
  class: mockup
  axes:
    - name: sim_mono
      velocity: 1000
      acceleration: 10
      steps_per_unit: 1000
      unit: deg
```

**NOTE** in case the angle/energy conversion is not done: `bm16mono.xtal_change("Si220")`.