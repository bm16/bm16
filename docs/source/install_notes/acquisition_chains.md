# Quick and dirty notes on acquisition chains, scans and synchronization devices at BM16

Here are kept few notes related to the acquisition chains on BM16, that is, type of scans and configuration of devices.

## Devices for synchronization and counting

### P201

Physically installed in `lbm16quartz`.

### MUSST

Installed in `lbm16quartz` via GPIB.

### OPIOM

Connected to `lbm16rubis` via the serial port `/dev/ttS0`.

```bash
sudo apt install setserial
sudo setserial -g /dev/ttyS0
sudo adduser blissadm dialout
sudo adduser blissadm tty
sudo reboot
```

## Step scans

## Continous scans (a.k.a. zap scans)

