#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Script to run the BM16 monochromator LN2 refill procedure regularly
"""
import sys
import time
import schedule
from bliss.config import static
from bm16.macros.monocryo import _logger, check_mono_ln2_level, refill_mono_procedure

sys.stdout.flush()
config = static.get_config()
session = config.get("monocryo")
session.setup()

LN2_MIN = 25  #: do refill at below this threshold


def refill_job():
    _logger.debug("schedule excuting job")
    refill_mono_procedure(ln2_threshold=LN2_MIN)


#: Run job every hour at minute 30
schedule.every().hour.at(":30").do(refill_job)

_logger.info("AUTOMATIC MONO REFILL MONITOR")
_logger.info(f"LOGFILE is {_logger._fname}")
_ = check_mono_ln2_level(LN2_MIN)

while True:
    schedule.run_pending()
    time.sleep(1)
